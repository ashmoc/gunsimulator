import Tools from "./Tools";
import { MENU, Configs } from "./Configs";
import dl from "./DL";
import Banner from "./Banner";
import Const from "./Taser/Const";

const { ccclass, property } = cc._decorator;

@ccclass

export default class GameMenu extends Tools {

    @property({ type: cc.Node, tooltip: '所有枪的父节点' })
    private gun_parent_node: cc.Node = null;

    @property({ type: cc.Label, tooltip: '金币的文字' })
    private lab_coin: cc.Label = null;

    @property({ type: cc.Node, tooltip: '购买界面' })
    private buy_panel: cc.Node = null;

    @property({ type: cc.Node, tooltip: '获取金币界面' })
    private get_coin_panel: cc.Node = null;

    private cur_gun_prise: number = 0;

    public ISDL: boolean = false;

    private cur_gun: cc.Node = null;

    private static _instance: any;
    public static get Instance(): GameMenu {
        if (GameMenu._instance == null)
            GameMenu._instance = new GameMenu();
        return GameMenu._instance;
    }

    onLoad() {
        this.coinActive();
    }

    coinActive() {
        if (cc.sys.localStorage.getItem('Coin') != null && cc.sys.localStorage.getItem('Coin') != "nan" && cc.sys.localStorage.getItem('Coin') != "") {
            this.lab_coin.string = cc.sys.localStorage.getItem('Coin');
        } else {
            this.lab_coin.string = "0";
        }
    }

    onLock(event) {
        // console.log(event.target.name + "ZZZZZZZZ");
        Banner.Instance.CreateVideo(function () {
            event.target.active = false;
            console.log("视频回调")
        });

        // this.onLockBack(event.target);
    }

    onLockBack(e: any) {
        // let event = e;
        e.active = false;
        // console.log("视频回调");
        // e = null;
    }

    start() {
        this.buy_panel.active = false;
        this.get_coin_panel.active = false;

        if (!this.GetData('gun_data')) {
           
        }
        this.InitGunData();
        this.WriteData();

        let gun_data = this.GetData('gun_data');
        for (let i = 0; i < this.gun_parent_node.childrenCount; i++) {
            const element = this.gun_parent_node.children[i];
            console.log(element.getChildByName('lock').name);
            console.log([element.name] + gun_data[element.name]);

            element.getChildByName('lock').active = gun_data[element.name]['lock'];
            element.getChildByName('lock').getChildByName('Lab_Sale').
                getComponent(cc.Label).string = gun_data[element.name]['price'];
            element.on(cc.Node.EventType.TOUCH_START, (event) => {
                if (!event.target.getChildByName('lock').active) {
                    Configs.gun_name = event.target.name;
                    //加载该枪支
                    let loading = this.LoadScene(MENU.GAME_LOADING, cc.find('Canvas'));
                    loading.then((_value: cc.Node) => {
                        _value.getComponent('GameLoading').load(MENU.GAME_PLAY);
                        this.RemoveScene('GameMenu');
                    })
                }//else {
                //     if(event.target.getChildByName('lock').getChildByName('coin').active) {
                //         this.cur_gun = event.target;
                //         this.buy_panel.active = true;
                //         this.buy_panel.getChildByName('Btn_Close').active = false;
                //         this.buy_panel.scale = 0;
                //         this.buy_panel.runAction(cc.scaleTo(0.3, 1));
                //         this.cur_gun_prise = parseInt(event.target.getChildByName('lock').getChildByName('Lab_Sale').getComponent(cc.Label).string);
                //         this.buy_panel.getChildByName('Lab_Tips').getComponent(cc.Label).string = 
                //             '是否花费' + this.cur_gun_prise + '金币解锁该枪支';
                //         this.unschedule(this.showBuyPanel);
                //         this.scheduleOnce(this.showBuyPanel, 3);
                //     }else {
                //         event.target.getChildByName('lock').active = false;
                //         let gun_data = GameMenu.Instance.GetData('gun_data');
                //         gun_data[event.target.name]['lock'] = false;
                //         GameMenu.Instance.WriteData();

                //     }
                // }
            }, this);
        }
    }

    Btn_Click(event, data) {
        switch (event.target.name) {
            case 'Btn_BackHome':
                cc.director.loadScene(Const.SceneName.Menu);

                // let loading = this.LoadScene(MENU.GAME_LOADING, cc.find('Canvas'));
                // loading.then( (_value : cc.Node) => {
                //     _value.getComponent('GameLoading').load(MENU.GAME_START);
                //     this.RemoveScene('GameMenu');
                // })
                // GameMenu.Instance.ISDL = true;
                break;
            case 'Btn_Forword':

                if (this.gun_parent_node.x > 960 - this.gun_parent_node.width) {
                    this.gun_parent_node.x -= 1920;
                } else {
                    this.gun_parent_node.x = -960;
                }
                break;
            case 'Btn_Back':

                if (this.gun_parent_node.x < -960) {
                    this.gun_parent_node.x += 1920;
                } else {
                    ``
                    this.gun_parent_node.x = 960 - this.gun_parent_node.width;
                }
                break;
            case 'Btn_Coin':
                Configs.video_name = 'btn_coin';
                this.PlayVideo((success: boolean) => {
                    if (Configs.video_name == 'btn_coin' && success) {
                        Configs.video_name = '';
                        Configs.coin += Configs.add_coin;
                        this.addCoin(Configs.add_coin, this.lab_coin);
                    }
                })
                break;
            case 'Btn_Buy':
                // if(Configs.coin >= this.cur_gun_prise) {

                //     this.addCoin(-this.cur_gun_prise, this.lab_coin);
                //     this.cur_gun.getChildByName('lock').active = false;
                //     let gun_data = this.GetData('gun_data');
                //     gun_data[this.cur_gun.name]['lock'] = false;
                //     this.WriteData();
                //     this.buy_panel.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc( (_node) => {
                //         _node.active = false;
                //     }, this)));
                // }else {
                //     event.target.parent.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc( (_node) => {
                //         _node.active = false;
                //     }, this)));
                //     this.get_coin_panel.active = true;
                //     this.get_coin_panel.scale = 0;
                //     this.get_coin_panel.runAction(cc.scaleTo(0.3, 1));
                //     this.get_coin_panel.getChildByName('Btn_Close').active = false;
                //     this.unschedule(this.showCoinPanel);
                //     this.scheduleOnce(this.showCoinPanel, 3);
                // }

                break;
            case 'Btn_Get_Coin':
                Banner.Instance.CreateVideo(this.GetCoinback);
                break;
            case 'Btn_Close':
                event.target.parent.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc((_node) => {
                    _node.active = false;
                }, this)));
                break;
            default:
                break;
        }
        if (this.cur_gun) {
            this.cur_gun = null;
        }
        if (this.cur_gun_prise > 0) {
            this.cur_gun_prise = 0;
        }
    }
    GetCoinback() {
        Configs.video_name = 'btn_get_coin';
        GameMenu.Instance.PlayVideo((success: boolean) => {
            if (Configs.video_name == 'btn_get_coin' && success) {
                Configs.video_name = '';
                Configs.coin += Configs.add_coin;
                GameMenu.Instance.addCoin(Configs.add_coin, GameMenu.Instance.lab_coin);
                GameMenu.Instance.get_coin_panel.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc((_node) => {
                    _node.active = false;
                }, GameMenu.Instance)));
            }
        })
    }

    Btn_buy(event) {
        if (parseInt(cc.sys.localStorage.getItem("Coin")) >= this.cur_gun_prise) {
            console.log(cc.sys.localStorage.getItem("Coin"));
            // this.addCoin(-this.cur_gun_prise, this.lab_coin);
            let a = parseInt(cc.sys.localStorage.getItem("Coin"));
            var b = a - this.cur_gun_prise;
            cc.sys.localStorage.setItem("Coin", b);
            this.cur_gun.getChildByName('lock').active = false;
            let gun_data = this.GetData('gun_data');
            gun_data[this.cur_gun.name]['lock'] = false;
            this.WriteData();
            this.buy_panel.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc((_node) => {
                _node.active = false;
            }, this)));
        } else {
            event.target.parent.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc((_node) => {
                _node.active = false;
            }, this)));
            this.get_coin_panel.active = true;
            this.get_coin_panel.scale = 0;
            this.get_coin_panel.runAction(cc.scaleTo(0.3, 1));
            this.get_coin_panel.getChildByName('Btn_Close').active = false;
            this.unschedule(this.showCoinPanel);
            this.scheduleOnce(this.showCoinPanel, 3);
        }
    }
    private showCoinPanel(): void {
        this.get_coin_panel.getChildByName('Btn_Close').active = true;

    }

    private showBuyPanel(): void {
        this.buy_panel.getChildByName('Btn_Close').active = true;

    }
}
