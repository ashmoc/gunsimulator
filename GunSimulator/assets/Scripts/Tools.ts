import { Configs } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Tools extends cc.Component {

    public PlayVideo (cb : (success : boolean) => void) {

        cb(true);
    }

    public PlayShare (cb : (success : boolean) => void) {

        cb(true);
    }



    protected static savData : any = {
        'last_into_game_time' : 0, 
        'last_day' : 0, 
        'lucky_count' : 3, 
        'gun_data' : {},
        'coin' : 0,
        'bullet' : 0,
    };
    protected static fileName : string = 'gun_save';


    //获取文件名
    protected GetSavFileName () : string {
        return Tools.fileName;
    }

    //获取数据
    protected GetData (_keyName) : any{
        return Tools.savData[_keyName];
    }

    //设置一个数据
    protected SetData (_keyName, _value) : void {
        Tools.savData[_keyName] = _value;
    }

    //存档
    protected WriteData () : void {
        cc.sys.localStorage.setItem(this.GetSavFileName(), JSON.stringify(Tools.savData));
    }

    //读档
    protected ReadData () : void {
        let objPrase = cc.sys.localStorage.getItem(this.GetSavFileName());
        if (objPrase) 
        {
            Tools.savData = JSON.parse(objPrase);
        }else{
            this.InitData();
        }
        console.log(objPrase);

    }

    protected InitData () : void {
        // let time = new Date();
        // let date = time.getMonth() + '.' + time.getDate();
        this.SetData('last_into_game_time', '');
        this.SetData('last_day', 0);
        this.SetData('lucky_count', 3);
        this.SetData('coin', 0);
        this.SetData('bullet', 0);
        this.InitGunData();
        this.WriteData();
    }

    protected InitGunData () : void {
        /**
         *  'init_bullet' : 12, 'residue_bullet': 12
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 100, 'residue_bullet': 100
            'init_bullet' : 1, 'residue_bullet': 1
            'init_bullet' : 5, 'residue_bullet': 5
            'init_bullet' : 100, 'residue_bullet': 100
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 12, 'residue_bullet': 12
            'init_bullet' : 30, 'residue_bullet': 30
            'init_bullet' : 12, 'residue_bullet': 12
            'init_bullet' : 30, 'residue_bullet': 30
         */
        let gun_data : any = {
            'P226' : {'lock' : false, 'price' : 1000,},
            'G36C' : {'lock' : true, 'price' : 1000,},
            'Remington' : {'lock' : true, 'price' : 1000,},
            'M60' : {'lock' : true, 'price' : 1000,},
            'M203' : {'lock' : true, 'price' : 1000,},
            'Crna strela' : {'lock' : true, 'price' : 1000,},
            'Vulcan' : {'lock' : true, 'price' : 1000,},
            'Ak47' : {'lock' : false, 'price' : 1000,},
            'M4A1' : {'lock' : true, 'price' : 1000,},
            'G36C_1' : {'lock' : true, 'price' : 6000,},
            'P226_1' : {'lock' : true, 'price' : 9000,},
            'Remington_1' : {'lock' : true, 'price' : 12000,},
            'P226_2' : {'lock' : true, 'price' : 15000,},
            'Ak47_1' : {'lock' : true, 'price' : 18000,},
        }
        this.SetData('gun_data', gun_data);
    }

    protected randNum (min, max) : number {
        let range = 1 + max - min;
        if(range <= 0)
        {
            range = 1;
        }
        return parseInt(Math.random() * (max - min+1) + min, 10); 
    }

    //获取manager
    protected getMain () : any {
        let manager = cc.find("Canvas").getComponent("GameMain");
        if(manager) {
            return manager;
        }
        return null;
    }

    //加载场景
    protected LoadScene (url : string, _parent : cc.Node) {
        return new Promise((resolve, reject) => {
            cc.loader.loadRes(url, cc.Prefab, function(err, res) {
                if(err) {
                    resolve(null);
                    console.log('没有该场景', url);
                    return;
                }
                let scene = cc.instantiate(res);
                _parent.addChild(scene);
                resolve(scene);
            })
        });
    }

    //删除场景
    protected RemoveScene (sceneName : string, _parent: cc.Node = cc.find('Canvas')) : void {
        let scene = _parent.getChildByName(sceneName);
        if(scene){
            scene.destroy();
        }
    }

    protected addCoin (_coin : number, lab : cc.Label) : void {
        Configs.coin += _coin;
        if(Configs.coin < 0) {
            Configs.coin = 0;
        }
        lab.string = '' + Configs.coin;
        this.SetData('coin', Configs.coin);
        this.WriteData();
    }
    protected addBullet (_bullet : number, lab : cc.Label) : void {
        Configs.bullet += _bullet;
        lab.string = 'x' + Configs.bullet;
        this.SetData('bullet', Configs.bullet);
        this.WriteData();
    }
    
    //找到未解锁的枪支
    protected unLock () : any {
        let lock_gun_arr : any = [];
        let gun_data = this.GetData('gun_data');
        for (const gun in gun_data) {
            if(gun_data[gun]['lock']) {
                lock_gun_arr.push(gun);
            }
        }
        return lock_gun_arr;
    }
}