const {ccclass, property} = cc._decorator;

@ccclass
export default class AudioManager extends cc.Component {

    private static instance : AudioManager = null;
    public static get Instance () : AudioManager {
        if(this.instance == null) {
            this.instance = new AudioManager();
        }
        return this.instance;
    }

    private audio_id : number = null;

    private sound_id : number = null;

    onLoad () : void {
        AudioManager.instance = this;
    }

    playMusic (audio : cc.AudioClip) : void {
        this.audio_id = cc.audioEngine.playMusic(audio, true);
    }

    resumeMusic () : void {
        cc.audioEngine.resumeMusic();
    }

    pauseMusic () : void {
        cc.audioEngine.pauseMusic();
    }

    playSound (sound : cc.AudioClip, isloop : boolean = false) : void {
        this.sound_id = cc.audioEngine.playEffect(sound, isloop);
    }

    pauseSound () : void {
        cc.audioEngine.setEffectsVolume(0);
    }

    resumeSound () : void {
        cc.audioEngine.setEffectsVolume(1);
    }

    stopSound () : void {
        cc.audioEngine.stopAllEffects();
    }

    getSoundVolume () : number {
        return cc.audioEngine.getEffectsVolume();
    }
}
