const {ccclass, property} = cc._decorator;

var POS = cc.Enum({
    default : 0,
    LEFT_UP : 1,
    LEFT_DOWN : 2,
    RIGHT_UP : 3,
    RIGHT_DOWN : 4
})




@ccclass
export default class UIAdaptive extends cc.Component {

    @property({type : POS})
    private pos = POS.default;

    onLoad () {
        console.log(cc.view.getScaleX(), cc.view.getScaleY());
        // 1. 先找到 SHOW_ALL 模式适配之后，本节点的实际宽高以及初始缩放值
        let srcScaleForShowAll = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height);
        let realWidth = this.node.width * (cc.view.getCanvasSize().width / this.node.width);
        let realHeight = this.node.height * (cc.view.getCanvasSize().height / this.node.height);

        // 2. 基于第一步的数据，再做缩放适配
        this.node.x *= cc.view.getCanvasSize().width / realWidth;
        this.node.y *= cc.view.getCanvasSize().height / realHeight;
        console.log(cc.view.getCanvasSize().width / realWidth, cc.view.getCanvasSize().height / realHeight);


        // let gauge_x : number = cc.view.getVisibleSize().width / 2 - (Math.abs(this.node.x) + this.node.width / 2);
        // let gauge_y : number = cc.view.getVisibleSize().height / 2 - (Math.abs(this.node.y) + this.node.height / 2);
        // console.log(gauge_x, gauge_y);
        // console.log(cc.view.getCanvasSize().width - gauge_x, cc.view.getCanvasSize().height - gauge_y);
        // this.node.x = (cc.view.getCanvasSize().width - gauge_x) * (this.node.x > 0 ? 1 : -1);
        // this.node.y = (cc.view.getCanvasSize().height - gauge_y) * (this.node.y > 0 ? 1 : -1);
        // switch (this.pos) {
        //     case POS.LEFT_UP:
                
        //         break;
        //     case POS.LEFT_DOWN:
                
        //         break;
        //     case POS.RIGHT_UP:
                
        //         break;
        //     case POS.RIGHT_DOWN:
                
        //         break;
        
        //     default:
        //         break;
        // }
        // this.node.x = -this.node.parent.width / 2 + (cc.view.getVisibleSize().width / 2 + this.node.x);
    }
}
