import GUN from "./GUN";
import { STATE, BOLT_STATE, MODE, CLIP, AUDIO } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class M60 extends GUN {

    @property({type : cc.Node})
    private pole : cc.Node = null;

    @property(cc.Sprite)
    private Order_1 : cc.Sprite = null;

    // private static _instance: any;
    // public static get Instance(): M60 {
    //     if (M60._instance == null)
    //     M60._instance = new M60();
    //     return M60._instance;
    // }
    // onLoad(){
    //     M60._instance = this;
    // }


    Change()
    {
        if (cc.sys.platform == cc.sys.ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing2", "()V");
        }
        var self = this;
            cc.loader.loadRes("Image/Gun/M60/new_1",cc.SpriteFrame, function (err, Frame)
        {
            self.Order_1.spriteFrame = Frame;
        })
       
        

    }


    //拉栓
    protected pullPlug () {
        this.bolt_node.getChildByName('bolt').stopAllActions();
        this.bolt_node.getChildByName('bolt').x = 0;
        this.bolt_node.getChildByName('bolt').runAction(cc.sequence(
            cc.moveTo(0.1, -this.bolt_node.getChildByName('bolt').width, 45.5),
            cc.moveTo(0.1, 117.6, 45.5),
            cc.callFunc((_node) => {_node.x = 117.6;}
        )));
    }

    update () {
        if(this.cur_touch_name == 'trigger') {
            //判断是否是连发
            if(this.isCanFire() && this.ani.getAnimationState('Recoil').time >= 0.08) {
                this.ani.play('Recoil');
                // this.ani.playAdditive('Fire');
                if(this.cur_bullet_count > 0) {
                    this.setBullet(this.cur_bullet_count - 1);
                    this.popup_Shell();
                    this.playSound(AUDIO.FIRE);
                    if(this.cur_bullet_count == 0) {
                        this.singleFireLight();
                    }else {
                        this.fireLight();
                    }
                }
            }
        }
    }


    //弹弹壳 
    protected popup_Shell () {
        this.pullPlug();
        let dir = this.randNum(0, 1) == 0 ? -1 : 1;
        let v1 = cc.v2(this.randNum(50, 200), this.randNum(500, 700));
        let v2 = cc.v2(this.randNum(v1.x, v1.x + 200), -1000);
        let _rotation = dir * this.randNum(70, 120);
        this.popup_Bullet(this.shell_pre, this.shell_spr, 0.4, v1, v2, _rotation);
    }

    protected onTouchStart (event)  : void {
        let pole_rect = null;
        let trigger_rect = null;
        let clip_rect = null;
        let magazine_rect = null;
        if(this.pole) {
            pole_rect = this.pole.getBoundingBoxToWorld();
            if(pole_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('pole');
                this.cur_touch_name = 'pole';
            }
        }
        if(this.trigger_node) {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if(trigger_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('trigger');
                this.cur_touch_name = 'trigger';
                this.trigger_node.rotation = this.trigger_rotation;
                if(this.isCanFire()) {
                    this.popup_Shell();
                    let clips = this.ani.getClips();
                    this.ani.play(clips[CLIP.RECOIL].name);
                    // this.ani.playAdditive(clips[CLIP.FIRE].name);
                    this.singleFireLight();
                    this.setBullet(this.cur_bullet_count - 1);
                    this.last_bullet_state = 'fire';
                    this.playSound(AUDIO.FIRE);
                }else {
                    this.playSound(AUDIO.TRIGGER);
                }
            }
        }
        if(this.clip_node) {
            clip_rect = this.clip_node.getBoundingBoxToWorld();
            if(clip_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('clip');
                this.cur_touch_name = 'clip';
            }
        }
        if(this.magazine_node) {
            magazine_rect = this.magazine_node.getBoundingBoxToWorld();
            if(magazine_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('magazine');
                this.cur_touch_name = 'magazine';
            }
        }
        this.touch_y = event.touch.getLocation().y;super.onTouchStart(event);
    }

    protected onTouchMove (event) : void {
        switch (this.cur_touch_name) {
            case 'pole':
                let delta = event.touch.getDelta();
                let touch_x = this.pole.parent.convertToNodeSpaceAR(event.touch.getLocation()).x;
                if(delta.x < 0) {
                    if(this.bolt_node.x > -this.bolt_node.width + 30) {
                        if(touch_x < this.pole.x) {
                            this.bolt_node.x = touch_x;
                            this.pole.x = this.bolt_node.x;
                        }
                    }else {
                        if(this.cur_bullet_count > 0 && !this.bolt_is_finished) {
                            if(this.cur_bole_state == BOLT_STATE.NOBOLE) {
                                this.cur_bole_state = BOLT_STATE.BOLE;
                            }
                            if(!this.bolt_is_finished) {
                                this.playSound(AUDIO.BOLTBACK);
                                this.bolt_is_finished = true;
                            }
                        }
                        this.bolt_node.x = -this.bolt_node.width + 30;
                        this.pole.x = this.bolt_node.x;
                    }
                }else {
                    if(this.bolt_node.x <= 0) {
                        if(touch_x > this.pole.x) {
                            this.bolt_node.x = touch_x;
                            this.pole.x = this.bolt_node.x;
                        }
                    }else {
                        if(this.bolt_is_finished) {
                            this.playSound(AUDIO.BOLTFOR);
                            this.bolt_is_finished = false;
                        }
                        this.bolt_node.x = 0.1;
                        this.pole.x = 0.1;
                    }
                }
                break;
        
            default:
                break;
        }
    }

    protected onTouchEnd (event) : void {
        switch (this.cur_touch_name) {
            case 'pole':
                this.bolt_node.runAction(cc.moveTo(0.1, 0, this.bolt_node.y));
                this.pole.runAction(cc.moveTo(0.1, 0, this.pole.y));
                this.playSound(AUDIO.BOLTFOR);
                //如果枪栓拉到底了
                if(this.bolt_is_finished) {
                    this.bolt_is_finished = false;
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }
                break;
            case 'trigger':
                this.trigger_node.rotation = 0;
                this.fireLight(false);
                break;
            case 'clip':
                if(this.cur_magazine_state == STATE.MAGAZINE) {
                    this.ani.play('DownMagazine');
                    setTimeout(() => {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(0);
                }else{
                    this.ani.play('UpMagazine');
                    this.playSound(AUDIO.UPMAGAZINE);
                    this.cur_magazine_state = STATE.MAGAZINE;
                    this.setBullet(this.max_bullet_count);
                }
                break;
            case 'magazine':
                //上下弹匣
                if(this.touch_y - event.touch.getLocation().y > 0) {
                    if(this.cur_magazine_state == STATE.MAGAZINE) {
                        this.ani.play('DownMagazine');
                        setTimeout(() => {
                            this.playSound(AUDIO.DOWNMAGAZINE);
                        }, 300);
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                }else {
                    if(this.cur_magazine_state == STATE.NOMAGAZINE) {
                        this.ani.play('UpMagazine');
                        this.playSound(AUDIO.UPMAGAZINE);
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);

                    }
                }
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }
}
