import GUN from "./GUN";
import { MODE, STATE, BOLT_STATE, CLIP, AUDIO } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class P226 extends GUN {


    @property(cc.Node) 
    private bolt_1 : cc.Node = null;

    @property(cc.Node)
    private lock : cc.Node = null;

    @property(cc.Node)
    private board : cc.Node = null;

    @property(cc.Sprite)
    private Order_1 : cc.Sprite = null;
    @property(cc.Sprite)
    private Order_2 : cc.Sprite = null;


    // private static _instance: any;
    // public static get Instance(): P226 {
    //     if (P226._instance == null)
    //     P226._instance = new P226();
    //     return P226._instance;
    // }
    // onLoad(){
    //     P226._instance = this;
    // }


    //拉栓
    protected pullPlug () {
        this.bolt_node.stopAllActions();
        this.bolt_1.stopAllActions();
        this.bolt_node.x = 0;
        this.bolt_1.x = 0;
        let _x = 0;
        if(this.cur_bullet_count == 1) {
            _x = -260;
        }
        this.bolt_node.runAction(cc.sequence(
            cc.moveBy(0.1, -285, 0),
            cc.moveTo(0.1, _x, 0),
            cc.callFunc((_node) => {_node.x = _x;}
        )));
        this.bolt_1.runAction(cc.sequence(
            cc.moveBy(0.1, -285, 0),
            cc.moveTo(0.1, _x, 0),
            cc.callFunc((_node) => {_node.x = _x;}
        )));
    }

        Change()
        {
            if (cc.sys.platform == cc.sys.ANDROID) {
                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing2", "()V");
            }
            var self = this;
                console.log("进入回调");
                cc.loader.loadRes("Image/Gun/P226/new_1",cc.SpriteFrame, function (err, Frame)
                {
                    self.Order_1.spriteFrame = Frame;
                })
    
                cc.loader.loadRes("Image/Gun/P226/new_2",cc.SpriteFrame, function (err, Frame)
                {
                    self.Order_2.spriteFrame = Frame;
                })
           
            
        }

    protected onTouchStart (event) : void {
        let trigger_rect = null;
        if(this.trigger_node) {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if(trigger_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('trigger', this.cur_bullet_count);
                if(this.cur_bullet_count <=0){
                   
                
                }
                this.cur_touch_name = 'trigger';
                if(this.cur_bullet_count == 1) {
                    this.board.y = 15;
                    this.trigger_node.rotation = this.trigger_rotation / 2;
                    this.lock.stopAllActions();
                    this.lock.runAction(cc.rotateTo(0.1, -60));
                }else if(this.cur_bullet_count != 1 && this.board.y == 0) {
                    this.trigger_node.rotation = this.trigger_rotation;
                    this.lock.stopAllActions();
                    this.lock.runAction(cc.sequence(cc.rotateTo(0.1, -60), cc.callFunc((_node) =>{_node.rotation = 0;}, this)))
                }
                if(this.isCanFire()) {
                    this.popup_Shell();
                    let clips = this.ani.getClips();
                    this.ani.play(clips[CLIP.RECOIL].name);
                    // this.ani.playAdditive(clips[CLIP.FIRE].name);
                    this.singleFireLight();
                    this.playSound(AUDIO.FIRE);
                    this.setBullet(this.cur_bullet_count - 1);
                    this.last_bullet_state = 'fire';
                }else {
                    this.playSound(AUDIO.TRIGGER);
                }
            }
        }
        super.onTouchStart(event);
        if(this.cur_touch_name == 'bolt') {
            if(this.board.y > 0) {
                this.board.y = 0;
                if(this.cur_bullet_count > 0) {
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }
            }
        }
        let lock_rect = null;
        if(this.lock) {
            lock_rect = this.lock.getBoundingBoxToWorld();
            if(lock_rect.contains(event.touch.getLocation())) {
                if(this.board.y == 0) {
                    if(this.lock.rotation == 0) {
                        this.lock.rotation = -60;
                        this.trigger_node.rotation = this.trigger_rotation / 2;
                    }else {
                        this.lock.rotation = 0;
                        this.trigger_node.rotation = 0;
                    }
                }
            }
        }
    }

    protected onTouchMove (event) : void {
        if(this.cur_touch_name == 'bolt') {
            let delta = event.touch.getDelta();
            let touch = this.bolt_node.parent.convertToNodeSpaceAR(event.touch.getLocation());
            if(delta.x < 0) {
                if(this.bolt_node.x > -285) {
                    if(this.bolt_node.getBoundingBox().contains(touch)) {
                        this.bolt_node.x += delta.x;
                        this.bolt_1.x += delta.x;
                    }
                }else {
                    if(this.cur_bullet_count > 0 && !this.bolt_is_finished) {
                        if(this.cur_bole_state == BOLT_STATE.NOBOLE) {
                            this.cur_bole_state = BOLT_STATE.BOLE;
                        }else {
                            this.setBullet(this.cur_bullet_count - 1);
                            let dir = this.randNum(0, 1) == 0 ? -1 : 1;
                            let v1 = cc.v2(dir * this.randNum(100, 300), this.randNum(500, 700));
                            let v2 = cc.v2(dir * this.randNum(Math.abs(v1.x), Math.abs(v1.x) + 300), -1000);
                            let _rotation = dir * this.randNum(30, 80);
                            this.popup_Bullet(this.bullet_pre, this.bullet_spr, 0.4, v1, v2, _rotation);
                        }
                        this.bolt_is_finished = true;
                        this.playSound(AUDIO.BOLTBACK);
                    }
                    this.bolt_node.x = -285;
                    this.bolt_1.x = -285;
                    this.lock.rotation = -60;
                    this.trigger_node.stopAllActions();
                    if(this.trigger_node.rotation != this.trigger_rotation) {
                        this.trigger_node.runAction(cc.rotateTo(0.1, this.trigger_rotation / 2));
                    }
                }
            }else {
                if(this.bolt_node.x < 0) {
                    if(this.bolt_node.getBoundingBox().contains(touch)) {
                        this.bolt_node.x += delta.x;
                        this.bolt_1.x += delta.x;
                    }
                }else {
                    if(this.bolt_is_finished) {
                        this.playSound(AUDIO.BOLTFOR);
                        this.bolt_is_finished = false;
                    }
                    this.bolt_node.x = 0;
                    this.bolt_1.x = 0;
                }
            }
        }
    }

    protected onTouchEnd (event) : void {
        switch (this.cur_touch_name) {
            case 'bolt':
                if(this.bolt_is_finished) {
                    this.bolt_is_finished = false;
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }
                this.playSound(AUDIO.BOLTFOR);
                this.bolt_node.runAction(cc.moveTo(0.1, 0, 0));
                this.bolt_1.runAction(cc.moveTo(0.1, 0, 0));
                break;
            case 'lock':
                //改变模式
                if(this.touch_y - event.touch.getLocation().y < 1) {
                    if(this.mode == MODE.SINGLE) {
                        this.lock_node.rotation += this.lock_rotation;
                        this.mode = MODE.LOCK;
                    }
                }else if(this.touch_y - event.touch.getLocation().y > -1){
                    if(this.mode == MODE.LOCK) {
                        this.lock_node.rotation -= this.lock_rotation;
                        this.mode = MODE.SINGLE;
                    }
                }
                this.playSound(AUDIO.LOCK);
                break;
            case 'trigger':
                if(this.board.y > 0) {
                    this.trigger_node.rotation = this.trigger_rotation / 2;
                }else {
                    this.trigger_node.rotation = 0;
                }
                break;
            case 'clip':
                //上下弹匣
                    if(this.cur_magazine_state == STATE.MAGAZINE) {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                        this.magazine_node.stopAllActions();
                        this.magazine_node.runAction(cc.moveTo(0.2, 20, -133));
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                    else {
                        setTimeout(() => {
                            this.playSound(AUDIO.UPMAGAZINE);
                        }, 300);
                        this.magazine_node.stopAllActions();
                        this.magazine_node.runAction(cc.moveTo(0.5, 160, 520));
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);
                    }
                break;
            case 'magazine':
                //上下弹匣
                if(this.touch_y - event.touch.getLocation().y > 0) {
                    if(this.cur_magazine_state == STATE.MAGAZINE) {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                        this.magazine_node.stopAllActions();
                        this.magazine_node.runAction(cc.moveTo(0.2, 20, -133));
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                }else {
                    if(this.cur_magazine_state == STATE.NOMAGAZINE) {
                        setTimeout(() => {
                            this.playSound(AUDIO.UPMAGAZINE);
                        }, 300);
                        this.magazine_node.stopAllActions();
                        this.magazine_node.runAction(cc.moveTo(0.5, 160, 520));
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);
                    }
                }
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }

}
