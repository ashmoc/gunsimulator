import Tools from "./Tools";
import { MENU } from "./Configs";
import Banner from "./Banner";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GamePlay extends Tools {

    @property({ type: cc.Prefab, tooltip: '所有枪的预制体' })
    private gun_pre_arr: Array<cc.Prefab> = new Array<cc.Prefab>();

    @property({ type: cc.Node, tooltip: '枪的父节点' })
    private gun_parent: cc.Node = null;

    private gun_node: cc.Node = null;

    start() {
        console.log(this.gun_pre_arr);
        Banner.Instance.CreateCustomAd();
    }

    public loadGun(gun_name: string): void {
        for (let i = 0; i < this.gun_pre_arr.length; i++) {
            const element = this.gun_pre_arr[i];
            if (element.name == gun_name) {
                this.gun_node = cc.instantiate(this.gun_pre_arr[i]);
                this.gun_parent.addChild(this.gun_node);
                return;
            }
        }
    }

    public Btn_Click(event, data): void {
        switch (event.target.name) {
            case 'Btn_BackHome':
                let loading = this.LoadScene(MENU.GAME_LOADING, cc.find('Canvas'));
                loading.then((_value: cc.Node) => {
                    _value.getComponent('GameLoading').load(MENU.GAME_MENU);
                    this.RemoveScene('GamePlay');
                })
                break;

            default:
                break;
        }
    }

    private index: number = 0;
    public AddCount(): void {
        this.index++;
        if (this.index >= 15) {
            this.index = 0;
        }
    }
}
