import Tools from "./Tools";
import { MODE, STATE, BOLT_STATE, CLIP, AUDIO, Configs } from "./Configs";
import AudioManager from "./AudioManager";
import GameMenu from "./GameMenu";
import TaserManager from "./Taser/TaserManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GUN extends Tools {

    @property({ type: cc.Integer, tooltip: '最大子弹数目' })
    protected max_bullet_count: number = 30;

    protected cur_bullet_count: number = 0;

    // @property({tooltip : '是否有枪栓'})
    // protected is_bole : boolean = true;

    @property({ tooltip: '是否有锁止' })
    protected is_lock: boolean = true;

    @property({ type: cc.Prefab, tooltip: '子弹壳的预制体' })
    protected shell_pre: cc.Prefab = null;

    @property({ type: cc.Prefab, tooltip: '子弹的预制体' })
    protected bullet_pre: cc.Prefab = null;

    @property({ type: cc.Node, tooltip: '子弹的节点' })
    protected bullet_node: cc.Node = null;

    @property({ type: cc.SpriteFrame, tooltip: '子弹的图片' })
    protected bullet_spr: cc.SpriteFrame = null;

    @property({ type: cc.SpriteFrame, tooltip: '子弹壳的图片' })
    protected shell_spr: cc.SpriteFrame = null;

    @property({ type: cc.Node, tooltip: '枪栓' })
    protected bolt_node: cc.Node = null;

    @property({ type: cc.Node, tooltip: '扳机' })
    protected trigger_node: cc.Node = null;

    @property({ type: cc.Node, tooltip: '弹匣按钮' })
    protected clip_node: cc.Node = null;

    @property({ type: cc.Node, tooltip: '弹匣' })
    protected magazine_node: cc.Node = null;

    @property({ type: cc.Node, tooltip: '锁止' })
    protected lock_node: cc.Node = null;

    @property({ type: cc.Node, tooltip: '火光节点' })
    protected fire_light_node: cc.Node = null;

    @property({ type: cc.SpriteFrame, tooltip: '火光的图片' })
    protected fire_light_spr_arr: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();

    @property({ type: cc.AudioClip, tooltip: '音效' })
    protected clip_arr: Array<cc.AudioClip> = new Array<cc.AudioClip>();

    @property({ type: cc.Float, tooltip: '扳机旋转的角度' })
    protected trigger_rotation: number = 0;

    @property({ type: cc.Float, tooltip: '锁止旋转的角度' })
    protected lock_rotation: number = 0;

    @property({ type: cc.ParticleSystem, tooltip: '粒子' })
    protected particle_node: cc.ParticleSystem = null;

    protected touch_y: number = 0;

    protected mode: any = MODE.LOCK;

    protected cur_magazine_state: any = STATE.NOMAGAZINE;

    protected cur_bole_state: any = BOLT_STATE.NOBOLE;

    protected ani: cc.Animation = null;

    protected cur_touch_name: string = '';

    protected bolt_is_finished: boolean = false;       //枪栓是否拉到底了

    protected last_bullet_state: string = '';          //上一发子弹的状态

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    }

    start() {
        this.init();
    }

    protected isCanFire(): boolean {
        if (this.is_lock) {
            if (this.mode != MODE.LOCK && this.cur_bullet_count > 0 && this.cur_bole_state == BOLT_STATE.BOLE) {
                return true;
            }
        } else if (this.cur_bullet_count > 0 && this.cur_bole_state == BOLT_STATE.BOLE) {
            return true;
        }
        return false;
    }

    protected onTouchStart(event): void {
        let bolt_rect = null;
        let trigger_rect = null;
        let clip_rect = null;
        let magazine_rect = null;
        let lock_rect = null;
        if (this.bolt_node) {
            bolt_rect = this.bolt_node.getBoundingBoxToWorld();
            if (bolt_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('bolt');
                this.cur_touch_name = 'bolt';
            }
        }
        if (this.trigger_node) {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if (trigger_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('trigger');
                this.cur_touch_name = 'trigger';
                this.trigger_node.rotation = this.trigger_rotation;
                if (this.isCanFire()) {
                    this.popup_Shell();
                    let clips = this.ani.getClips();
                    // this.particle_node.resetSystem();
                    this.ani.play(clips[CLIP.RECOIL].name);
                    this.singleFireLight();
                    // this.ani.playAdditive(clips[CLIP.FIRE].name);
                    this.setBullet(this.cur_bullet_count - 1);
                    this.last_bullet_state = 'fire';
                    this.playSound(AUDIO.FIRE);
                } else {
                    this.playSound(AUDIO.TRIGGER);
                    TaserManager.showGunTipPanel();
                }
            }
        }
        if (this.clip_node) {
            clip_rect = this.clip_node.getBoundingBoxToWorld();
            if (clip_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('clip');
                this.cur_touch_name = 'clip';
            }
        }
        if (this.magazine_node) {
            magazine_rect = this.magazine_node.getBoundingBoxToWorld();
            if (magazine_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('magazine');
                this.cur_touch_name = 'magazine';
            }
        }
        if (this.lock_node) {
            lock_rect = this.lock_node.getBoundingBoxToWorld();
            if (lock_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('lock');
                this.cur_touch_name = 'lock';
            }
        }
        this.touch_y = event.touch.getLocation().y;
    }

    protected onTouchMove(event): void { }

    protected onTouchEnd(event): void { }

    //拉栓
    protected pullPlug() {
        this.bolt_node.stopAllActions();
        this.bolt_node.x = 0;
        this.bolt_node.getChildByName('bullet').active = false;
        this.bolt_node.runAction(cc.sequence(
            cc.moveBy(0.1, -this.bolt_node.width, 0),
            cc.callFunc((_node) => { _node.getChildByName('bullet').active = true; }),
            cc.moveTo(0.1, 0, 0),
            cc.callFunc((_node) => { _node.x = 0; _node.getChildByName('bullet').active = false; }
            )));
    }

    //弹子弹
    protected popup_Bullet(_prefab: cc.Prefab, spr: cc.SpriteFrame, time: number, v2_1: cc.Vec2, v2_2: cc.Vec2, _rotation: number) {
        let bullet = cc.instantiate(_prefab);
        bullet.getChildByName('Img').getComponent(cc.Sprite).spriteFrame = spr;
        this.bullet_node.addChild(bullet);
        var bezier = [cc.v2(0, 0), v2_1, v2_2];
        var bezierTo = cc.bezierTo(time, bezier);
        let act = cc.sequence(cc.spawn(cc.rotateBy(time, _rotation), bezierTo), cc.callFunc(() => { bullet.destroy(); }));
        bullet.runAction(act);
    }

    //弹弹壳 
    protected popup_Shell() {
        if (this.is_lock) {
            this.pullPlug();
            let dir = this.randNum(0, 1) == 0 ? -1 : 1;
            let v1 = cc.v2(this.randNum(50, 200), this.randNum(500, 700));
            let v2 = cc.v2(this.randNum(v1.x, v1.x + 200), -1000);
            let _rotation = dir * this.randNum(70, 120);
            this.popup_Bullet(this.shell_pre, this.shell_spr, 0.4, v1, v2, _rotation);
        }
    }

    protected isContinuous(): boolean {
        if (this.mode == MODE.CONTINUOUS) {
            if (this.ani.getAnimationState('Recoil').time >= 0.08) {
                return true;
            }
        }
        return false;
    }

    protected fireLight(is_show: boolean = true) {
        if (is_show && this.fire_light_spr_arr.length > 0) {
            this.fire_light_node.getComponent(cc.Sprite).spriteFrame = this.fire_light_spr_arr[this.randNum(0, this.fire_light_spr_arr.length - 1)];
        } else {
            this.fire_light_node.getComponent(cc.Sprite).spriteFrame = null;
        }
    }

    protected singleFireLight() {
        this.fire_light_node.opacity = 125;
        this.fireLight();
        this.fire_light_node.runAction(cc.sequence(cc.fadeIn(0.1), cc.callFunc((_node) => { this.fireLight(false); })));
    }

    protected init(): void {
        this.ani = this.node.getChildByName('Animation Holder').getComponent(cc.Animation);
        // this.max_bullet_count = this.GetData('gun_data')[this.node.name]['residue_bullet'];
        this.max_bullet_count += Configs.bullet;
        this.setBullet(0);
    }



    //设置子弹数量
    public setBullet(bulletCount: number) {
        if (cc.sys.platform == cc.sys.ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing", "()V");
        }
        this.cur_bullet_count = bulletCount;
        this.node.parent.parent.getChildByName('Lab_Bullet').getComponent(cc.Label).string = this.cur_bullet_count + '/' + this.max_bullet_count;
    }

    protected playSound(_index: number): void {
        AudioManager.Instance.playSound(this.clip_arr[_index]);
    }
}