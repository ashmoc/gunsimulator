import GUN from "./GUN";
import { STATE, BOLT_STATE, MODE, CLIP, AUDIO } from "./Configs";
import AudioManager from "./AudioManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class M4A1 extends GUN {

    @property({type : cc.Node})
    private pole : cc.Node = null;

    @property(cc.Sprite)
    private Order_1 : cc.Sprite = null;
    @property(cc.Sprite)
    private Order_2 : cc.Sprite = null;


    // private static _instance: any;
    // public static get Instance(): M4A1 {
    //     if (M4A1._instance == null)
    //     M4A1._instance = new M4A1();
    //     return M4A1._instance;
    // }
    // onLoad(){
    //     M4A1._instance = this;
    // }

    Change()
    {
        if (cc.sys.platform == cc.sys.ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing2", "()V");
        }
        var self =this;
            cc.loader.loadRes("Image/Gun/Remington/new_1",cc.SpriteFrame, function (err, Frame)
            {
                self.Order_1.spriteFrame = Frame;
            })
    
            cc.loader.loadRes("Image/Gun/Remington/new_2",cc.SpriteFrame, function (err, Frame)
            {
                self.Order_2.spriteFrame = Frame;
            })
       
        
    }
        
    update () {
        this.pole.x = this.bolt_node.x;
        if(this.cur_touch_name == 'trigger') {
            //判断是否是连发
            if(this.isCanFire() && this.isContinuous()) {
                this.ani.play('Recoil');
                // this.ani.playAdditive('Fire');
                if(this.cur_bullet_count > 0) {
                    this.setBullet(this.cur_bullet_count - 1);
                    this.popup_Shell();
                    this.playSound(AUDIO.FIRE);
                    if(this.cur_bullet_count == 0) {
                        this.singleFireLight();
                    }else {
                        this.fireLight();
                    }
                }
            }
        }
    }

    //弹弹壳 
    protected popup_Shell () {
        // this.pullPlug();
        let dir = this.randNum(0, 1) == 0 ? -1 : 1;
        let v1 = cc.v2(this.randNum(50, 200), this.randNum(500, 700));
        let v2 = cc.v2(this.randNum(v1.x, v1.x + 200), -1000);
        let _rotation = dir * this.randNum(70, 120);
        this.popup_Bullet(this.shell_pre, this.shell_spr, 0.4, v1, v2, _rotation);
    }

    protected onTouchStart (event)  : void {
        let pole_rect = null;
        let trigger_rect = null;
        let clip_rect = null;
        let magazine_rect = null;
        let lock_rect = null;
        if(this.pole) {
            pole_rect = this.pole.getBoundingBoxToWorld();
            if(pole_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('pole');
                this.cur_touch_name = 'pole';
            }
        }
        if(this.trigger_node) {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if(trigger_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('trigger');
                this.cur_touch_name = 'trigger';
                this.trigger_node.rotation = this.trigger_rotation;
                if(this.isCanFire()) {
                    this.popup_Shell();
                    this.playSound(AUDIO.FIRE);
                    this.singleFireLight();
                    let clips = this.ani.getClips();
                    this.ani.play(clips[CLIP.RECOIL].name);
                    // this.ani.playAdditive(clips[CLIP.FIRE].name);
                    this.setBullet(this.cur_bullet_count - 1);
                    this.last_bullet_state = 'fire';
                }else{
                    this.playSound(AUDIO.TRIGGER);
                }
            }
        }
        if(this.clip_node) {
            clip_rect = this.clip_node.getBoundingBoxToWorld();
            if(clip_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('clip');
                this.cur_touch_name = 'clip';
            }
        }
        if(this.magazine_node) {
            magazine_rect = this.magazine_node.getBoundingBoxToWorld();
            if(magazine_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('magazine');
                this.cur_touch_name = 'magazine';
            }
        }
        if(this.lock_node) {
            lock_rect = this.lock_node.getBoundingBoxToWorld();
            if(lock_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('lock');
                this.cur_touch_name = 'lock';
            }
        }
        this.touch_y = event.touch.getLocation().y;super.onTouchStart(event);
    }

    protected onTouchMove (event) : void {
        switch (this.cur_touch_name) {
            case 'pole':
                let delta = event.touch.getDelta();
                let touch_x = this.pole.parent.convertToNodeSpaceAR(event.touch.getLocation()).x;
                if(delta.x < 0) {
                    if(this.bolt_node.x > -this.bolt_node.width + 30) {
                        if(touch_x < this.pole.x) {
                            this.bolt_node.x = touch_x;
                            this.pole.x = this.bolt_node.x;
                        }
                    }else {
                        if(this.cur_bullet_count > 0 && !this.bolt_is_finished) {
                            if(this.cur_bole_state == BOLT_STATE.NOBOLE) {
                                this.cur_bole_state = BOLT_STATE.BOLE;
                            }else {
                                this.setBullet(this.cur_bullet_count - 1);
                                let dir = this.randNum(0, 1) == 0 ? -1 : 1;
                                let v1 = cc.v2(dir * this.randNum(100, 300), this.randNum(500, 700));
                                let v2 = cc.v2(dir * this.randNum(Math.abs(v1.x), Math.abs(v1.x) + 300), -1000);
                                let _rotation = dir * this.randNum(30, 80);
                                this.popup_Bullet(this.bullet_pre, this.bullet_spr, 0.4, v1, v2, _rotation);
                            }
                            this.bolt_node.getChildByName('bullet').active = true;
                        }
                        if(!this.bolt_is_finished) {
                            this.bolt_is_finished = true;
                            this.playSound(AUDIO.BOLTBACK);
                        }
                        this.bolt_node.x = -this.bolt_node.width + 30;
                        this.pole.x = this.bolt_node.x;
                    }
                }else {
                    if(this.bolt_node.x <= 0) {
                        if(touch_x > this.pole.x) {
                            this.bolt_node.x = touch_x;
                            this.pole.x = this.bolt_node.x;
                        }
                    }else {
                        if(this.bolt_is_finished) {
                            this.bolt_is_finished = false;
                            this.playSound(AUDIO.BOLTFOR);
                        }
                        this.bolt_node.getChildByName('bullet').active = false;
                        this.bolt_node.x = 0.1;
                        this.pole.x = 0.1;
                    }
                }
                break;
        
            default:
                break;
        }
    }

    protected onTouchEnd (event) : void {
        switch (this.cur_touch_name) {
            case 'pole':
                this.bolt_node.runAction(cc.moveTo(0.1, 0, this.bolt_node.y));
                this.pole.runAction(cc.moveTo(0.1, 0, this.pole.y));
                this.playSound(AUDIO.BOLTFOR);
                //如果枪栓拉到底了
                if(this.bolt_is_finished) {
                    this.bolt_is_finished = false;
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }
                break;
            case 'trigger':
                this.trigger_node.rotation = 0;
                break;
            case 'clip':
                if(this.cur_magazine_state == STATE.MAGAZINE) {
                    this.ani.play('DownMagazine');
                    setTimeout(() => {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(0);
                }else{
                    this.ani.play('UpMagazine');
                    this.playSound(AUDIO.UPMAGAZINE);
                    this.cur_magazine_state = STATE.MAGAZINE;
                    this.setBullet(this.max_bullet_count);
                }
                break;
            case 'magazine':
                //上下弹匣
                if(this.touch_y - event.touch.getLocation().y > 0) {
                    if(this.cur_magazine_state == STATE.MAGAZINE) {
                        this.ani.play('DownMagazine');
                        setTimeout(() => {
                            this.playSound(AUDIO.DOWNMAGAZINE);
                        }, 300);
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                }else {
                    if(this.cur_magazine_state == STATE.NOMAGAZINE) {
                        this.ani.play('UpMagazine');
                        this.playSound(AUDIO.UPMAGAZINE);
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);
                    }
                }
                break;
            case 'lock':
                //改变模式
                if(this.touch_y - event.touch.getLocation().y > 1) {
                    if(this.mode < MODE.SINGLE) {
                        this.lock_node.rotation += this.lock_rotation;
                        this.mode++;
                    }
                }else if(this.touch_y - event.touch.getLocation().y < -1){
                    if(this.mode > MODE.LOCK) {
                        this.lock_node.rotation -= this.lock_rotation;
                        this.mode--;
                    }
                }
                this.playSound(AUDIO.LOCK);
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }
}
