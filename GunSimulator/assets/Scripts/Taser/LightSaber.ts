import Const from "./Const";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LightSaber extends cc.Component {
    saberNd: cc.Node = null;
    saberSp: cc.Sprite = null;
    handleSp: cc.Sprite = null;
    audioSource: cc.AudioSource = null;
    saberMaterial: cc.Material = null;

    glowColorSize: number = 0.12;
    glowThreshold: number = 0.6;

    interval: any = null;

    Init(index: number) {
        this.saberNd = this.node.getChildByName("Saber");
        this.saberSp = cc.find("Saber/SaberSp", this.node).getComponent(cc.Sprite);
        this.handleSp = this.node.getChildByName("HandleSp").getComponent(cc.Sprite);
        this.audioSource = this.node.getComponent(cc.AudioSource);
        this.saberMaterial = this.saberSp.getMaterial(0);;
        this.saberNd.scaleY = 0;

        //加载光剑
        cc.resources.load(`${Const.Path.LightSaberSp}${index}`, cc.SpriteFrame, (err: any, spriteFrame: any) => {
            if (err) return;
            this.handleSp.spriteFrame = spriteFrame;
        });
    }


    UseLishtSaber(isUse: boolean) {
        cc.Tween.stopAllByTarget(this.node);
        cc.Tween.stopAllByTarget(this.saberNd);


        if (isUse) {
            cc.resources.load(`${Const.Path.LightSaberSwingAudio}`, cc.AudioClip, (err: any, res: any) => {
                if (err) return;
                this.audioSource.clip = res;
                this.audioSource.loop = false;
                this.audioSource.play();
            });

            cc.tween(this.node).to(0.78, { scale: 0.7, position: cc.v3(0, -50) }).start();

            cc.tween(this.saberNd).to(0.78, { scaleY: 1 }).call(() => {
                this.interval = setInterval(() => {
                    this.glowColorSize = this.glowColorSize == 0.12 ? 0.14 : 0.12;
                    this.glowThreshold = this.glowThreshold == 0.6 ? 0.9 : 0.6;
                    let material: cc.Material = this.saberSp.getMaterial(0);
                    material.setProperty("glowColorSize", this.glowColorSize);
                    material.setProperty("glowThreshold", this.glowThreshold);
                    this.saberSp.setMaterial(0, material);
                }, 50);

                cc.resources.load(`${Const.Path.LightSaberAudio}`, cc.AudioClip, (err: any, res: any) => {
                    if (err) return;
                    this.audioSource.clip = res;
                    this.audioSource.loop = true;
                    this.audioSource.play();
                });
            }).start();

        } else {
            if (this.interval) {
                clearInterval(this.interval);
            }

            cc.tween(this.node).to(0.2, { scale: 1, position: cc.v3(0, 50) }).call(() => {
                this.audioSource.stop();
            }).start();

            cc.tween(this.saberNd).to(0.2, { scaleY: 0 }).start();
        }
    }

    SetColor(color: string) {
        let material: cc.Material = this.saberSp.getMaterial(0);
        material.setProperty("glowColor", this.saberSp.node.color.fromHEX(color));
        this.saberSp.setMaterial(0, material);
    }

}
