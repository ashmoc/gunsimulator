import Banner from "../../Scripts/Banner";
import Const from "./Const";
import policyPanel from "./policyPanel";
import { resourceUtil } from "./resourceUtil";
import StorageManager from "./StorageManager";
import Tip from "./Tip";
import { poolManager } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuManager extends cc.Component {

    policyPanel: policyPanel = null;

    onLoad() {
        //cc.assetManager.loadBundle("Sound");

        //cc.director.loadScene("GameMain");

        Banner.Instance.showBanner();

        this.policyPanel = cc.find("PolicyPanel", this.node).getComponent(policyPanel);

        if (!StorageManager.getBool(Const.Key.AgreePolicy)) {
            this.policyPanel.show(true);
        }
    }

    OnMenuButton1Click() {
        cc.director.loadScene("GameMain");
    }

    OnMenuButton2Click() {
        cc.director.loadScene(Const.SceneName.Taser);
    }

    OnMenuButton3Click() {
        cc.director.loadScene(Const.SceneName.LightSaber);
    }

    OnPolicyButtonClick() {
        this.policyPanel.show(true);
    }

    public static showTip(content: string | number, callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.Tip).then((prefab: any) => {
            let tip = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tip.getComponent(Tip).show(content, callback);
        });
    }
}
