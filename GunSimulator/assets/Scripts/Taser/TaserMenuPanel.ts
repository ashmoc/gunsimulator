import Const from "./Const";
import { resourceUtil } from "./resourceUtil";
import TaserItem from "./TaserItem";
import { ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TaserMenuPanel extends cc.Component {
    private static _instance: TaserMenuPanel = null!;
    constructor() {
        super();
        TaserMenuPanel._instance = this;
    }

    public static get Instance() {
        return TaserMenuPanel._instance;
    }

    contentNd: cc.Node = null!;
    contentLayout: cc.Layout = null!;

    taserItems: TaserItem[] = [];

    onLoad() {
        this.contentNd = cc.find("MenuScrollView/view/content", this.node);
        this.contentLayout = this.contentNd.getComponent(cc.Layout);

        //自适应高度
        let paddingTop = this.node.getContentSize().height / 2 - this.contentLayout.spacingY / 2 - this.contentLayout.cellSize.height;
        this.contentLayout.paddingTop = paddingTop;

        //自适应大小
        let height = this.contentNd.getContentSize().height;
        let columnCount = Math.ceil(Const.TaserCount / 2);
        let width = this.contentLayout.cellSize.width * columnCount + this.contentLayout.spacingX * (columnCount - 1) + this.contentLayout.paddingLeft + this.contentLayout.paddingRight;
        this.contentNd.setContentSize(width, height);

        //实例化菜单
        let loadCount = Const.TaserCount;

        Const.TaserArray.forEach(element => {
            resourceUtil.loadPrefab(Const.Path.TaserItem).then((prefab: any) => {
                let taserItem = ZTool.Instantiate(prefab, this.contentNd).getComponent(TaserItem);
                taserItem.init(element);
                this.taserItems.push(taserItem);

                loadCount -= 1;
                if (loadCount === 0) {
                    //加载完成调用刷新菜单方法
                    this.refreshMenuBtns(0);
                }
            });
        });

    }

    showPanel(active: boolean) {
        this.node.active = active;
    }

    refreshMenuBtns(index: number) {
        if (!this.taserItems) return;

        for (let i = 0; i < this.taserItems.length; i++) {
            this.taserItems[i].refresh(index);
        }
    }

    onReturnBtnClick() {
        cc.director.loadScene(Const.SceneName.Menu);
    }

}
