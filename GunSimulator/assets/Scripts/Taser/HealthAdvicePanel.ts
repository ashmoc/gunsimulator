const { ccclass, property } = cc._decorator;

enum AgeLimit {
    Eight,
    Twelve,
    Sixteen
}

@ccclass
export default class HealthAdvicePanel extends cc.Component {
    @property({ type: cc.Enum(AgeLimit), displayName: `适龄`, tooltip: `适龄限制` })
    ageLimit: AgeLimit = AgeLimit.Eight;

    @property({ displayName: `著作权人` })
    company: string = ``;

    @property({ displayName: `登记号` })
    licenseNumber: string = ``;

    ageLimitAtalsPath: string = `Common/AgeLimit`;

    start() {
        this.node.getChildByName(`CompanyLb`).getComponent(cc.Label).string = `${this.company}`;
        this.node.getChildByName(`LicenseLb`).getComponent(cc.Label).string = `${this.licenseNumber}`;

        //适龄提示角标
        let ageLimitSp: cc.Sprite = this.node.getChildByName(`AgeLimitSp`).getComponent(cc.Sprite);
        switch (this.ageLimit) {
            case AgeLimit.Eight:
                cc.loader.loadRes(this.ageLimitAtalsPath, cc.SpriteAtlas, (err, res: cc.SpriteAtlas) => {
                    ageLimitSp.spriteFrame = res.getSpriteFrame(`8`);
                });
                break;
            case AgeLimit.Twelve:
                cc.loader.loadRes(this.ageLimitAtalsPath, cc.SpriteAtlas, (err, res: cc.SpriteAtlas) => {
                    ageLimitSp.spriteFrame = res.getSpriteFrame(`12`);
                });
                break;
            case AgeLimit.Sixteen:
                cc.loader.loadRes(this.ageLimitAtalsPath, cc.SpriteAtlas, (err, res: cc.SpriteAtlas) => {
                    ageLimitSp.spriteFrame = res.getSpriteFrame(`16`);
                });
                break;
        }
    }

}
