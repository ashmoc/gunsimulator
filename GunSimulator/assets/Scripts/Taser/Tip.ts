import { poolManager } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Tip extends cc.Component {
    maskNd: cc.Node = null!;
    contentNd: cc.Node = null!;
    contentLb: cc.Label = null!;

    onLoad() {
        this.maskNd = this.node.getChildByName(`Mask`);
        this.contentNd = this.node.getChildByName(`Content`);
        this.contentLb = this.contentNd.getComponent(cc.Label);
    }

    public show(content: string, callback: Function = () => { }) {
        this.contentLb.string = `${content}`;
        //@ts-ignore
        this.contentLb._forceUpdateRenderData(true);

        this.maskNd.setContentSize(this.contentNd.getContentSize().width + 200, this.contentNd.getContentSize().height + 10);

        this.showTween();
    }

    showTween() {
        this.node.scale = 0;
        this.node.opacity = 255;
        this.node.setPosition(cc.Vec2.ZERO);

        cc.Tween.stopAllByTarget(this.node);

        cc.tween(this.node).to(0.5, { scale: 1 }, { easing: `elasticOut` })
            .to(0.25, { position: cc.v3(0, 150), scale: 0, opacity: 0 })
            .call(() => {
                poolManager.Instance.putNode(this.node);
            }).start();
    }

}
