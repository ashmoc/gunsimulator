import Banner from "../../Scripts/Banner";
import Const from "./Const";
import { resourceUtil } from "./resourceUtil";
import StorageManager from "./StorageManager";
import Taser from "./Taser";
import TaserManager from "./TaserManager";
import TaserMenuPanel from "./TaserMenuPanel";
import TipPanel from "./TipPanel";
import { ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TaserItem extends cc.Component {
    bgSp: cc.Sprite = null!;
    button: cc.Button = null!;
    taserNd: cc.Node = null!;
    lockNd: cc.Node = null!;

    index: number;
    unLock: boolean = true;

    init(index: number) {
        this.index = index;

        this.bgSp = this.node.getChildByName("BG").getComponent(cc.Sprite);
        this.button = this.node.getComponent(cc.Button);
        this.taserNd = this.node.getChildByName("Taser");
        this.lockNd = this.node.getChildByName("Lock");

        //按钮回调
        this.button.clickEvents = [];
        let clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;//对应的组件node
        clickEventHandler.component = `TaserItem`;  //对应的组件脚本
        clickEventHandler.handler = `onBtnClick`;        //回调
        //clickEventHandler.customEventData = itemData;     //数据
        this.button.clickEvents.push(clickEventHandler);

        //加载电击枪
        resourceUtil.loadPrefab(`${Const.Path.Taser}${index}`).then((prefab: any) => {
            let taser = ZTool.Instantiate(prefab, this.taserNd).getComponent(Taser);
            taser.Init(cc.v2(0.25, 0.25));
        });

        this.refresh(index);
    }

    refresh(index: number) {
        this.unLock = StorageManager.getTypeUnlock(Const.Key.Taser, this.index);
        this.lockNd.active = !this.unLock;

        //加载背景
        cc.resources.load(`${Const.Path.ItemBG}${Number(this.unLock)}`, cc.SpriteFrame, (err: any, spriteFrame: any) => {
            if (err) return;
            this.bgSp.spriteFrame = spriteFrame;
        });
    }

    unlock() {
        StorageManager.setTypeUnlock(Const.Key.Taser, this.index);
        this.unLock = StorageManager.getTypeUnlock(Const.Key.Taser, this.index);
        TaserMenuPanel.Instance.refreshMenuBtns(this.index);
    }

    onBtnClick() {
        if (!this.unLock) {
            TaserManager.showTipPanel(this.index, () => {
                Banner.Instance.CreateVideo(() => { this.unlock(); });
            });
            return;
        }

        TaserManager.Instance.showMainPanel(this.index);
    }

}