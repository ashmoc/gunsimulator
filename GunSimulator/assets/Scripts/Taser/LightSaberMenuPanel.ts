import Const from "./Const";
import LightSaberItem from "./LightSaberItem";
import { resourceUtil } from "./resourceUtil";
import { ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LightSaberMenuPanel extends cc.Component {
    private static _instance: LightSaberMenuPanel = null!;
    constructor() {
        super();
        LightSaberMenuPanel._instance = this;
    }

    public static get Instance() {
        return LightSaberMenuPanel._instance;
    }

    contentNd: cc.Node = null!;
    contentLayout: cc.Layout = null!;

    lightSaberItems: LightSaberItem[] = [];

    onLoad() {
        this.contentNd = cc.find("MenuScrollView/view/content", this.node);
        this.contentLayout = this.contentNd.getComponent(cc.Layout);

        //自适应高度
        let paddingTop = this.node.getContentSize().height / 2 - this.contentLayout.spacingY / 2 - this.contentLayout.cellSize.height;
        this.contentLayout.paddingTop = paddingTop;

        //自适应大小
        let height = this.contentNd.getContentSize().height;
        let columnCount = Math.ceil(Const.LightSaberCount / 2);
        let width = this.contentLayout.cellSize.width * columnCount + this.contentLayout.spacingX * (columnCount - 1) + this.contentLayout.paddingLeft + this.contentLayout.paddingRight;
        this.contentNd.setContentSize(width, height);

        //实例化菜单
        let loadCount = Const.LightSaberCount;

        Const.LightSaberArray.forEach(element => {
            resourceUtil.loadPrefab(Const.Path.LightSaberItem).then((prefab: any) => {
                let lightSaberItem = ZTool.Instantiate(prefab, this.contentNd).getComponent(LightSaberItem);
                lightSaberItem.init(element);
                this.lightSaberItems.push(lightSaberItem);

                loadCount -= 1;
                if (loadCount === 0) {
                    //加载完成调用刷新菜单方法
                    this.refreshMenuBtns();
                }
            });
        });

    }

    showPanel(active: boolean) {
        this.node.active = active;
    }

    refreshMenuBtns() {
        if (!this.lightSaberItems) return;

        for (let i = 0; i < this.lightSaberItems.length; i++) {
            this.lightSaberItems[i].refresh();
        }
    }

    onReturnBtnClick() {
        cc.director.loadScene(Const.SceneName.Menu);
    }

}
