import Banner from "../../Scripts/Banner";
import Const from "./Const";
import MenuManager from "./MenuManager";
import StorageManager from "./StorageManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class policyPanel extends cc.Component {
    toggle: cc.Toggle = null!;
    scrollView: cc.ScrollView = null!;

    start() {
        this.toggle = cc.find("ScrollView/Toggle", this.node).getComponent(cc.Toggle);
        this.scrollView = cc.find("ScrollView", this.node).getComponent(cc.ScrollView);

        this.toggle.isChecked = false;
        this.scrollView.content.height = 20000;
    }

    onBtnClick() {
        if (this.toggle.isChecked) {
            StorageManager.setBool(Const.Key.AgreePolicy, true);

            this.show(false);
            //Banner.Instance.showBanner();
        } else {
            MenuManager.showTip(`您需要先同意隐私政策。`);
        }
    }

    public show(active: boolean) {
        this.node.active = active;
        if (!active) {
            //Banner.Instance.showBanner();
        }
    }

}
