const { ccclass, property } = cc._decorator;

@ccclass
export default class MenuButton extends cc.Component {
    onLoad() {
        cc.Tween.stopAllByTarget(this.node);

        let time = Math.random() * 1000;

        setTimeout(() => {
            cc.tween(this.node).to(1.5, { scale: 0.8 }).to(1.5, { scale: 0.75 }).union().repeatForever().start();
        }, time);
    }
}
