export default class Const {

    public static TaserCount = 5;
    public static LightSaberCount = 11;

    public static Key = {
        Taser: "KEY_TASER",
        LightSaber: "KEY_LIGHTSABER",
        AgreePolicy: "AGREEPOLICY11"
    }

    public static TaserArray = [0, 1, 2, 3, 4];
    public static LightSaberArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    // Red Magenta Blue Indigo Green Yellow
    public static ColorArray = ["#ff0000", "#ff00ff", "#0041ff", "#10d0d0", "#24b926", "#d4d522"];

    public static SceneName = {
        Menu: "Menu",
        LightSaber: "LightSaber",
        Taser: "Taser",
    }

    public static BundleName = {
        Taser: "Taser",
    }

    public static Tip = {
        BatteryLowTip: "电量耗尽，请充电后再使用。",
        GunTip: "请重新插上弹夹,打开保险并拉栓使用。"
    }

    public static Path = {
        Taser: "Taser/Taser_",
        TaserItem: "Taser/TaserItem",
        TaserSp: "Taser/Images/Taser_",

        ItemBG: "Taser/Sprites/ItemBg_",

        LightSaber: "Taser/LightSaber",
        LightSaberItem: "Taser/LightSaberItem",
        LightSaberSp: "Taser/Sprites/LightSaber_",
        LightSaberAudio: "Taser/Sounds/LightSaber",
        LightSaberSwingAudio: "Taser/Sounds/LightSaberSwing",

        ColorItem: "Taser/ColorItem",

        TipPanel: "Taser/TipPanel",
        Tip: "Taser/Tip",
    }

}

export enum ColorType {
    Red = "#ff0000",
    Magenta = "#ff00ff",
    Blue = "#0041ff",
    Indigo = "#10d0d0",
    Green = "#24b926",
    Yellow = "#d4d522"
}
