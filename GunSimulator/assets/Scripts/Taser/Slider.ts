import { ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Slider extends cc.Component {

    fgSp: cc.Sprite = null;

    public set FillValue(value: number) {
        this.fgSp.fillRange = ZTool.Clamp(value, 0, 1);
    }

    onLoad() {
        this.fgSp = this.node.getChildByName("FG").getComponent(cc.Sprite);
    }
}
