import Const from "../Const";
import LightSaberMainPanel from "../LightSaberMainPanel";
import { resourceUtil } from "../resourceUtil";
import { ZTool } from "../ZTool";
import ColorItem from "./ColorItem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ColorManager extends cc.Component {

    layout: cc.Layout = null;

    onLoad() {
        //实例化菜单
        for (let i = 0; i < Const.ColorArray.length; i++) {
            resourceUtil.loadPrefab(Const.Path.ColorItem).then((prefab: any) => {
                let colorItem = ZTool.Instantiate(prefab, this.node).getComponent(ColorItem);
                colorItem.Init(Const.ColorArray[i]);
            });
        }

    }
}
