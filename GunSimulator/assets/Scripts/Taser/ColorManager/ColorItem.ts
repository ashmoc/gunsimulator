import Banner from "../../../Scripts/Banner";
import LightSaberMainPanel from "../LightSaberMainPanel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ColorItem extends cc.Component {

    colorNd: cc.Node = null;
    button: cc.Button = null!;

    color: string = "";

    Init(color: string) {
        this.colorNd = this.node.getChildByName("ColorSp");
        this.button = this.node.getComponent(cc.Button);
        this.color = color;

        this.colorNd.color = this.colorNd.color.fromHEX(color);

        //按钮回调
        this.button.clickEvents = [];
        let clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;//对应的组件node
        clickEventHandler.component = `ColorItem`;  //对应的组件脚本
        clickEventHandler.handler = `onBtnClick`;        //回调
        //clickEventHandler.customEventData = itemData;     //数据
        this.button.clickEvents.push(clickEventHandler);

    }

    onBtnClick() {
        Banner.Instance.CreateCustomAd();
        LightSaberMainPanel.Instance.setColor(this.color);
    }

}
