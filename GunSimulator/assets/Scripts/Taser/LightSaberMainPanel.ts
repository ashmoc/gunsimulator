import Const from "./Const";
import LightSaber from "./LightSaber";
import LightSaberMenuPanel from "./LightSaberMenuPanel";
import { resourceUtil } from "./resourceUtil";
import Slider from "./Slider";
import { poolManager, ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LightSaberMainPanel extends cc.Component {
    private static _instance: LightSaberMainPanel = null!;
    constructor() {
        super();
        LightSaberMainPanel._instance = this;
    }

    public static get Instance() {
        return LightSaberMainPanel._instance;
    }

    audio: cc.AudioSource = null;
    lightSaberNd: cc.Node = null;
    lightSaber: LightSaber = null;


    onLoad() {
        this.lightSaberNd = this.node.getChildByName("LightSaber");
        this.audio = this.node.getComponent(cc.AudioSource);

        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
    }

    touchStart(event) {
        if (this.lightSaber) {
            this.lightSaber.UseLishtSaber(true);
        }
    }

    touchEnd(event) {
        if (this.lightSaber) {
            this.lightSaber.UseLishtSaber(false);
        }
    }

    showPanel(active: boolean) {
        this.node.active = active;
    }

    setLightSaber(index: number) {
        if (this.lightSaber) {
            this.lightSaber.node.destroy();
            this.lightSaber = null;
        }

        resourceUtil.loadPrefab(`${Const.Path.LightSaber}`).then((prefab: any) => {
            this.lightSaber = ZTool.Instantiate(prefab, this.lightSaberNd).getComponent(LightSaber);
            this.lightSaber.Init(index);
        });
    }

    setColor(color: string) {
        if (this.lightSaber) {
            this.lightSaber.SetColor(color);
        }
    }

    onReturnBtnClick() {
        this.showPanel(false);
        LightSaberMenuPanel.Instance.showPanel(true);
    }

}
