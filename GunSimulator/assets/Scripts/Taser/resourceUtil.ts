const { ccclass, property } = cc._decorator;

@ccclass("resourceUtil")
export class resourceUtil {

    public static loadBundle(bundleName: string) {
        cc.assetManager.loadBundle(bundleName, (err, bundle) => {
            if (err) {
                return console.error(err);
            }
            // //此方法的参数与 cc.resources.load 相同
            // bundle.load(`prefab`, cc.Prefab, function (err, prefab) {
            //     let newNode = cc.instantiate(prefab);
            //     cc.director.getScene().addChild(newNode);
            // });

            // //加载场景
            // bundle.loadScene('test', function (err, scene) {
            //     cc.director.runScene(scene);
            // });
        });
    }

    public static loadBundleByName(bundleName: string) {
        cc.assetManager.loadBundle(bundleName);
    }


    /**
    * 加载资源
    * @param url   资源路径
    * @param type  资源类型
    * @param cb    回调
    * @method loadRes
    */
    public static loadRes(url: string, type: any, cb: Function = () => { }) {
        cc.resources.load(url, (err: any, res: any) => {
            if (err) {
                err(err.message || err);
                cb(err, res);
                return;
            }

            cb && cb(null, res);
        })
    }

    public static loadPrefab(path: string) {
        return new Promise((resolve, reject) => {
            this.loadRes(`${path}`, cc.Prefab, (err: any, prefab: cc.Prefab) => {

                if (err) {
                    console.error('effect load failed', path);
                    reject && reject();
                    return;
                }

                resolve && resolve(prefab);
            })
        })
    }

    public static loadSpriteFrameRes(path: string) {
        this.loadRes(`${path}`, cc.SpriteFrame, (err: any, sprite: cc.SpriteFrame) => {
            return sprite;
        })
    }

    public static loadJson(path: string) {
        return new Promise((resolve, reject) => {
            this.loadRes(`Jsons/${path}`, cc.JsonAsset, (err: any, json: cc.JsonAsset) => {
                if (err) {
                    reject && reject();
                    return;
                }

                resolve && resolve(json);
            })
        })
    }

}
