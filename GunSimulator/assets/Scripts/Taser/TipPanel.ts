import Banner from "../../Scripts/Banner";
import Const from "./Const";
import { resourceUtil } from "./resourceUtil";
import Taser from "./Taser";
import { poolManager, ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TipPanel extends cc.Component {
    tipNd: cc.Node = null;
    titleLb: cc.Label = null;
    tipLb: cc.Label = null;
    confirmLb: cc.Label = null;
    confirmVideoNd: cc.Node = null;
    equipNd: cc.Node = null;
    equipSp: cc.Sprite = null;
    taser: Taser = null;
    action: Function = null;

    onLoad() {
        this.tipNd = cc.find("TipSp", this.node);
        this.titleLb = cc.find("TipSp/TitleLb", this.node).getComponent(cc.Label);
        this.tipLb = cc.find("TipSp/TipLb", this.node).getComponent(cc.Label);
        this.confirmLb = cc.find("TipSp/ConfirmBtn/ConfirmBtnLb", this.node).getComponent(cc.Label);
        this.confirmVideoNd = cc.find("TipSp/ConfirmBtn/VideoSp", this.node);
        this.equipNd = cc.find("TipSp/Equip", this.node);
        this.equipSp = cc.find("TipSp/EquipSp", this.node).getComponent(cc.Sprite);
    }

    show(title: string, action: Function = null, scale: number = 1) {
        this.node.active = true;
        this.titleLb.string = `${title}`;
        this.tipLb.string = "";
        this.action = action;
        this.tipNd.scale = 0;
        this.equipNd.active = false;
        this.equipSp.spriteFrame = null;

        if (this.taser) {
            poolManager.Instance.putNode(this.taser.node);
            this.taser = null;
        }

        cc.tween(this.tipNd).to(0.3, { scale: scale }, { easing: `bounceOut` }).call(() => {
            Banner.Instance.CreateCustomAd();
        }).start();
    }

    showTaserTip(index: number, action: Function = null) {
        this.show("新装备", action);
        this.setConfirmBtn(true, "解锁装备");
        this.equipNd.active = true;

        //加载电击枪
        resourceUtil.loadPrefab(`${Const.Path.Taser}${index}`).then((prefab: any) => {
            this.taser = ZTool.Instantiate(prefab, this.equipNd).getComponent(Taser);
            this.taser.Init(cc.v2(0.2, 0.2));
        });
    }

    showBatteryLowTip(action: Function = null) {
        this.show("警告", action);
        this.setConfirmBtn(false, "充电");
        this.tipLb.string = Const.Tip.BatteryLowTip;
    }

    showLightSaberTip(index: number, action: Function = null) {
        this.show("新装备", action);
        this.setConfirmBtn(true, "解锁装备");
        this.equipNd.active = true;

        //加载光剑
        cc.resources.load(`${Const.Path.LightSaberSp}${index}`, cc.SpriteFrame, (err: any, spriteFrame: any) => {
            if (err) return;
            this.equipSp.spriteFrame = spriteFrame;
        });
    }

    showGunTip(action: Function = null) {
        this.show("提示", action, 2);
        this.tipLb.string = Const.Tip.GunTip;
        this.setConfirmBtn(false, "确认");
        this.equipNd.active = true;
    }

    setConfirmBtn(isWithVideoIcon: boolean, content: string) {
        this.confirmLb.string = content;
        this.confirmVideoNd.active = isWithVideoIcon;
        this.confirmLb.node.setPosition(isWithVideoIcon ? cc.v2(-40, 2) : cc.v2(0, 2));
    }

    hide() {

        cc.tween(this.tipNd).to(0.15, { scale: 0 }).call(() => {
            this.node.active = false;
            poolManager.Instance.putNode(this.node);
        }).start();
    }

    onConfirmBtnClick() {
        if (this.action) {
            this.action();
        }
        this.hide();

        // Banner.Instance.CreateVideo(
        // () => {
        //     if (this.action) {
        //         this.action();
        //     }
        //     this.hide();
        // }
        // );
    }

    onCancelBtnClick() {
        this.hide();
    }

}
