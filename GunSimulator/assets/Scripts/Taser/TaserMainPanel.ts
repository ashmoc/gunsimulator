import Banner from "../Banner";
import Const from "./Const";
import { resourceUtil } from "./resourceUtil";
import Slider from "./Slider";
import Taser from "./Taser";
import TaserManager from "./TaserManager";
import TaserMenuPanel from "./TaserMenuPanel";
import { ZTool } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TaserMainPanel extends cc.Component {
    private static _instance: TaserMainPanel = null!;
    constructor() {
        super();
        TaserMainPanel._instance = this;
    }

    public static get Instance() {
        return TaserMainPanel._instance;
    }

    flashNd: cc.Node = null;
    taserNd: cc.Node = null;
    returnBtnNd: cc.Node = null;
    taser: Taser = null;
    audio: cc.AudioSource = null;
    batterySlider: Slider = null;
    timer: any = null;

    isBatteryLow: boolean = false;

    private _batteryValue: number = 1;

    public set BatteryValue(value: number) {
        if (this.isBatteryLow) {
            return;
        }

        if (this._batteryValue <= 0) {
            this.useTaser(false);
            this.isBatteryLow = true;
            this._batteryValue = 0;

            TaserManager.showBatteryLowTip(() => { this.charge(); });
        }
        this._batteryValue = value;
    }
    public get BatteryValue() {
        return this._batteryValue;
    }

    onLoad() {
        this.flashNd = this.node.getChildByName("FlashSp");
        this.taserNd = this.node.getChildByName("Taser");
        this.returnBtnNd = this.node.getChildByName("ReturnBtn");
        this.batterySlider = this.node.getChildByName("BatterySlider").getComponent(Slider);
        this.audio = this.node.getComponent(cc.AudioSource);

        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);

    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
    }

    touchStart(event) {
        if (this.isBatteryLow) {
            TaserManager.showBatteryLowTip(() => { this.charge(); });
            return;
        }

        this.useTaser(true);
    }

    touchEnd(event) {
        this.useTaser(false);
    }

    showPanel(active: boolean) {
        this.node.active = active;
    }

    setTaser(index: number) {
        Banner.Instance.CreateCustomAd();

        this.BatteryValue = 1;
        this.batterySlider.FillValue = this.BatteryValue;

        if (this.taser) {
            this.taser.node.destroy();
        }

        //加载电击枪
        resourceUtil.loadPrefab(`${Const.Path.Taser}${index}`).then((prefab: any) => {
            this.taser = ZTool.Instantiate(prefab, this.taserNd).getComponent(Taser);
            this.taser.Init(cc.v2(0.5, 0.5));
        });

        this.charge();
    }

    useTaser(isUse: boolean) {
        this.returnBtnNd.active = !isUse;
        this.taser.Use(isUse);
        this.showFlash(isUse);
        this.reduceBattery(isUse);
    }

    showFlash(isShow: boolean) {
        this.flashNd.active = isShow;

        if (isShow) {
            this.audio.play();

            cc.tween(this.flashNd).to(0.1, { opacity: 0 }).to(0.05, { opacity: 200 }).union().repeatForever().start();
        } else {
            this.audio.stop();

            cc.Tween.stopAllByTarget(this.flashNd);

            this.flashNd.opacity = 0;
        }
    }

    charge() {
        this.isBatteryLow = false;
        this._batteryValue = 0.1;
        let batterytimer = setInterval(() => {
            this.BatteryValue += 0.05;
            this.batterySlider.FillValue = this.BatteryValue;
            if (this.BatteryValue >= 1) {
                clearInterval(batterytimer);
            }
        }, 10);
    }

    reduceBattery(isReduce: boolean) {
        if (isReduce) {
            this.timer = setInterval(() => {
                this.BatteryValue -= 0.02;
                this.batterySlider.FillValue = this.BatteryValue;
            }, 50);
        } else {
            if (this.timer) {
                clearInterval(this.timer);
            }
        }
    }

    onReturnBtnClick() {
        this.showPanel(false);
        TaserMenuPanel.Instance.showPanel(true);
    }

}
