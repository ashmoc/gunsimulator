import Const from "./Const";

const { ccclass, property } = cc._decorator;

@ccclass
export default class StorageManager extends cc.Component {

    public static ClearData() {
        cc.sys.localStorage.clear();
    }

    public static setItem(key: string, value: any) {
        cc.sys.localStorage.setItem(key, value);
    }

    public static getTypeUnlock(key: string, index: number) {
        key = `${key}${index}`;
        return Boolean(cc.sys.localStorage.getItem(key));
    }

    public static setTypeUnlock(key: string, index: number) {
        StorageManager.setItem(`${key}${index}`, 1);
    }

    public static getBool(key: string) {
        return Boolean(cc.sys.localStorage.getItem(`${key}`));
    }

    public static setBool(key: string, value: boolean) {
        cc.sys.localStorage.setItem(key, Number(value));
    }

}
