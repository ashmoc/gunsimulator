import Banner from "../../Scripts/Banner";
import Const from "./Const";
import { resourceUtil } from "./resourceUtil";
import StorageManager from "./StorageManager";
import TaserMainPanel from "./TaserMainPanel";
import TaserMenuPanel from "./TaserMenuPanel";
import Tip from "./Tip";
import TipPanel from "./TipPanel";
import { poolManager } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TaserManager extends cc.Component {
    private static _instance: TaserManager = null!;
    constructor() {
        super();
        TaserManager._instance = this;
    }

    public static get Instance(): TaserManager {
        return TaserManager._instance;
    }

    onLoad() {
        // cc.game.addPersistRootNode(this.node);
        // cc.director.getPhysicsManager().enabled = true;
        // cc.director.getPhysicsManager().debugDrawFlags = true;
        // cc.director.getCollisionManager().enabled = true;
        // cc.director.getCollisionManager().enabledDebugDraw = true;

        // cc.debug.setDisplayStats(false);

        StorageManager.setTypeUnlock(Const.Key.Taser, Const.TaserArray[0]);

        TaserMenuPanel.Instance.showPanel(true);
        TaserMainPanel.Instance.showPanel(false);
    }

    public showMainPanel(index: number) {
        TaserMenuPanel.Instance.showPanel(false);
        TaserMainPanel.Instance.showPanel(true);

        TaserMainPanel.Instance.setTaser(index);
    }

    public static showTipPanel(index: number, callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.TipPanel).then((prefab: any) => {
            let tipPanel = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tipPanel.getComponent(TipPanel).showTaserTip(index, callback);
        });
    }

    public static showGunTipPanel(callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.TipPanel).then((prefab: any) => {
            let tipPanel = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tipPanel.getComponent(TipPanel).showGunTip(callback);
        });
    }

    public static showBatteryLowTip(callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.TipPanel).then((prefab: any) => {
            let tipPanel = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tipPanel.getComponent(TipPanel).showBatteryLowTip(callback);
        });
    }

    public static showTip(content: string | number, callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.Tip).then((prefab: any) => {
            let tip = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tip.getComponent(Tip).show(content, callback);
        });
    }

}
