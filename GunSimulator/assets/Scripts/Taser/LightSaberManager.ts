import Const from "./Const";
import LightSaberMainPanel from "./LightSaberMainPanel";
import LightSaberMenuPanel from "./LightSaberMenuPanel";
import { resourceUtil } from "./resourceUtil";
import StorageManager from "./StorageManager";
import TaserMainPanel from "./TaserMainPanel";
import TaserMenuPanel from "./TaserMenuPanel";
import Tip from "./Tip";
import TipPanel from "./TipPanel";
import { poolManager } from "./ZTool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LightSaberManager extends cc.Component {
    private static _instance: LightSaberManager = null!;
    constructor() {
        super();
        LightSaberManager._instance = this;
    }

    public static get Instance(): LightSaberManager {
        return LightSaberManager._instance;
    }

    onLoad() {
        StorageManager.setTypeUnlock(Const.Key.LightSaber, Const.LightSaberArray[0]);

        LightSaberMenuPanel.Instance.showPanel(true);
        LightSaberMainPanel.Instance.showPanel(false);
    }

    public showMainPanel(index: number) {
        LightSaberMenuPanel.Instance.showPanel(false);
        LightSaberMainPanel.Instance.showPanel(true);
        LightSaberMainPanel.Instance.setLightSaber(index);
    }

    public static showTipPanel(index: number, callback: Function = () => { }) {
        resourceUtil.loadPrefab(Const.Path.TipPanel).then((prefab: any) => {
            let tipPanel = poolManager.Instance.getNode(prefab, cc.find("Canvas"));
            tipPanel.getComponent(TipPanel).showLightSaberTip(index, callback);
        });
    }

}
