const { ccclass, property } = cc._decorator;

@ccclass
export default class Taser extends cc.Component {

    taserOnNd: cc.Node = null;
    ani: cc.Animation = null;

    Init(scale: cc.Vec2) {
        this.taserOnNd = this.node.getChildByName("TaserOnSp");
        this.ani = this.node.getChildByName("Effect").getComponent(cc.Animation);

        this.node.setScale(scale);

        this.Use(false);
    }

    Use(isUse: boolean) {
        this.taserOnNd.active = isUse;
        this.ani.node.active = isUse;
        if (isUse) {
            this.ani.play();
        } else {
            this.ani.stop();
        }
    }

}
