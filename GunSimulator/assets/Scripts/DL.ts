import gamemenu from "./GameMenu";

const {ccclass, property} = cc._decorator;

@ccclass
export default class DL extends cc.Component {

    @property(cc.Node)
    DLPanel: cc.Node = null;



    private static _instance: any;
    public static get Instance(): DL {
        if (DL._instance == null)
            DL._instance = new DL();
        return DL._instance;
    }

    onLoad()
    {
        DL._instance = this;
        console.log("zzzzzzzzzz"+gamemenu.Instance.ISDL);
        if(gamemenu.Instance.ISDL == true){
            this.DLPanel.active =false;
        }
    }



    CloseDL(event,data)
    {
        switch(event.target.name){
            case "QQ":
                if (cc.sys.platform == cc.sys.ANDROID) {
                            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "DLQQ", "()V");
                        }
            break;
            case "WX":
                if (cc.sys.platform == cc.sys.ANDROID) {
                            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "DLWX", "()V");
                        }
            break;
        }
        
            cc.DLBack = function () {
                DL._instance.DLPanel.active = false;
                
        }
    }
}
