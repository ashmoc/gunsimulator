import GUN from "./GUN";
import { MODE, STATE, BOLT_STATE, AUDIO } from "./Configs";
import Banner from "./Banner";

const { ccclass, property } = cc._decorator;

@ccclass
export default class AK47 extends GUN {

    @property(cc.Sprite)
    private Order_1: cc.Sprite = null;
    @property(cc.Sprite)
    private Order_2: cc.Sprite = null;

    Change() {
        if (cc.sys.platform == cc.sys.ANDROID) {
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing2", "()V");
        } else {
            Banner.Instance.CreateCustomAd();
        }

        var self = this;
        cc.loader.loadRes("Image/Gun/AK47/new_1", cc.SpriteFrame, function (err, Frame) {
            self.Order_1.spriteFrame = Frame;
        })

        cc.loader.loadRes("Image/Gun/AK47/new_2", cc.SpriteFrame, function (err, Frame) {
            self.Order_2.spriteFrame = Frame;
        })


    }

    update() {
        if (this.cur_touch_name == 'trigger') {
            //判断是否是连发
            if (this.isCanFire() && this.isContinuous()) {
                this.ani.play('Recoil');
                // this.ani.playAdditive('Fire');
                if (this.cur_bullet_count > 0) {
                    this.setBullet(this.cur_bullet_count - 1);
                    this.popup_Shell();
                    this.playSound(AUDIO.FIRE);
                    if (this.cur_bullet_count == 0) {
                        this.singleFireLight();
                    } else {
                        this.fireLight();
                    }
                }
            }
        }
    }


    protected onTouchMove(event): void {
        let delta = event.touch.getDelta();
        switch (this.cur_touch_name) {
            case 'bolt':
                let touch_x = this.bolt_node.parent.convertToNodeSpaceAR(event.touch.getLocation()).x;
                //bolt的移动
                if (delta.x > 0) {
                    if (touch_x >= -this.bolt_node.width / 2) {
                        //如果手指往右 手指点小于bolt的最右边 bolt.x没有超过最右边 
                        if (this.bolt_node.x <= 0) {
                            //bolt移动后的位置没有超过最右边
                            if (this.bolt_node.x + delta.x < 0) {
                                this.bolt_node.x += delta.x;
                            } else {
                                if (this.bolt_is_finished) {
                                    this.bolt_is_finished = false;
                                    this.playSound(AUDIO.BOLTFOR);
                                }
                                this.bolt_node.x = 0;
                                this.bolt_node.getChildByName('bullet').active = false;
                            }
                        }
                    }
                } else {
                    if (touch_x <= this.bolt_node.width / 2) {
                        //如果手指往左 手指点大于bolt的最左边 bolt.x没有超过最左边 
                        if (this.bolt_node.x >= -this.bolt_node.width + 20) {
                            //bolt移动后的位置没有超过最左边
                            if (this.bolt_node.x + delta.x > -this.bolt_node.width + 20) {
                                this.bolt_node.x += delta.x;
                            } else {
                                if (this.cur_bullet_count > 0 && !this.bolt_is_finished) {
                                    if (!this.bolt_is_finished) {
                                        this.playSound(AUDIO.BOLTBACK);
                                        this.bolt_is_finished = true;
                                    }
                                    this.bolt_node.getChildByName('bullet').active = true;
                                    if (this.cur_bole_state == BOLT_STATE.NOBOLE) {
                                        this.cur_bole_state = BOLT_STATE.BOLE;
                                    } else {
                                        this.setBullet(this.cur_bullet_count - 1);
                                        let dir = this.randNum(0, 1) == 0 ? -1 : 1;
                                        let v1 = cc.v2(dir * this.randNum(100, 300), this.randNum(500, 700));
                                        let v2 = cc.v2(dir * this.randNum(Math.abs(v1.x), Math.abs(v1.x) + 300), -1000);
                                        let _rotation = dir * this.randNum(30, 80);
                                        this.popup_Bullet(this.bullet_pre, this.bullet_spr, 0.4, v1, v2, _rotation);
                                    }
                                }
                                this.bolt_node.x = -this.bolt_node.width + 20;
                            }
                        }
                    }
                }
            default:
                break;
        }
    }


    protected onTouchEnd(event): void {
        switch (this.cur_touch_name) {
            case 'bolt':
                this.bolt_node.runAction(cc.moveTo(0.1, 0, this.bolt_node.y));
                this.playSound(AUDIO.BOLTFOR);
                //如果枪栓拉到底了
                if (this.bolt_is_finished) {
                    this.bolt_is_finished = false;
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }
                break;
            case 'trigger':
                this.fireLight(false);
                this.particle_node.stopSystem();
                this.trigger_node.rotation = 0;
                break;
            case 'clip':
                if (this.cur_magazine_state == STATE.MAGAZINE) {
                    this.ani.play('DownMagazine');
                    setTimeout(() => {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(0);
                } else {
                    this.ani.play('UpMagazine');
                    this.playSound(AUDIO.UPMAGAZINE);
                    this.cur_magazine_state = STATE.MAGAZINE;
                    this.setBullet(this.max_bullet_count);
                }
                break;
            case 'magazine':
                //上下弹匣
                if (this.touch_y - event.touch.getLocation().y > 0) {
                    if (this.cur_magazine_state == STATE.MAGAZINE) {
                        this.ani.play('DownMagazine');
                        setTimeout(() => {
                            this.playSound(AUDIO.DOWNMAGAZINE);
                        }, 300);
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                } else {
                    if (this.cur_magazine_state == STATE.NOMAGAZINE) {
                        this.ani.play('UpMagazine');
                        this.playSound(AUDIO.UPMAGAZINE);
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);

                    }
                }
                break;
            case 'lock':
                //改变模式
                if (this.touch_y - event.touch.getLocation().y > 1) {
                    if (this.mode < MODE.SINGLE) {
                        this.lock_node.rotation += this.lock_rotation;
                        this.mode++;
                    }
                } else if (this.touch_y - event.touch.getLocation().y < -1) {
                    if (this.mode > MODE.LOCK) {
                        this.lock_node.rotation -= this.lock_rotation;
                        this.mode--;
                    }
                }
                this.playSound(AUDIO.LOCK);
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }
}