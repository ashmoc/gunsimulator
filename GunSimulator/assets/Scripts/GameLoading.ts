import Tools from "./Tools";
import { Configs } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameLoading extends Tools {

    load(url : string) {
        let scene = this.LoadScene(url, cc.find('Canvas'));
        scene.then( (value : cc.Node) => {
            if(value.name == 'GamePlay') {
                value.getComponent('GamePlay').loadGun(Configs.gun_name);
                Configs.gun_name = '';
            }
            this.RemoveScene('GameLoading');
        })
    }
}