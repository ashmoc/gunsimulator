import GUN from "./GUN";
import { MODE, STATE, BOLT_STATE, AUDIO } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CrnaStrela extends GUN {

    @property({type : cc.Node, tooltip : '枪栓'})
    private bolt : cc.Node = null;

    private is_bolt_back : boolean = true;

    private is_first_upmagazine : boolean = false;

    @property(cc.Sprite)
    private Order_1 : cc.Sprite = null;

    private static _instance: any;
    public static get Instance(): CrnaStrela {
        if (CrnaStrela._instance == null)
        CrnaStrela._instance = new CrnaStrela();
        return CrnaStrela._instance;
    }
    onLoad(){
        CrnaStrela._instance = this;
    }

    Change()
    {
        var self = this;
        cc.loader.loadRes("Image/Gun/Crna strela/new_1",cc.SpriteFrame, function (err, Frame)
        {
            CrnaStrela.Instance.Order_1.spriteFrame = Frame;
        })

    }

    //拉栓
    protected pullPlug () {
        this.bolt_node.stopAllActions();
        this.bolt.stopAllActions();
        this.bolt_node.x = 0;
        this.bolt.x = 0;
        this.bolt.getChildByName('bullet').active = false;
        this.bolt_node.runAction(cc.sequence(
            cc.moveBy(0.1, -this.bolt.width, 0),
            cc.moveTo(0.1, 0, 0),
            cc.callFunc((_node) => {_node.x = 0;}
        )));
        this.bolt.runAction(cc.sequence(
            cc.moveBy(0.1, -this.bolt.width, 0),
            cc.callFunc((_node) => {_node.getChildByName('bullet').active = true;}),
            cc.moveTo(0.1, 0, 0),
            cc.callFunc((_node) => {_node.x = 0;_node.getChildByName('bullet').active = false;}
        )));
    }

    protected onTouchStart(event) : void {
        super.onTouchStart(event);
        if(this.cur_touch_name == 'bolt') {
            this.bolt_node.getChildByName('on').active = true;
            this.bolt_node.getChildByName('off').active = false;
            this.playSound(AUDIO.LOCK);
        }
    }

    protected onTouchMove (event) : void {
        let delta = event.touch.getDelta();
        switch (this.cur_touch_name) {
            case 'bolt':
                let touch_x = this.bolt_node.parent.convertToNodeSpaceAR(event.touch.getLocation()).x;
                //bolt的移动
                if(delta.x < 0) {
                    if(this.bolt.x > -this.bolt.width + 120) {
                        if(touch_x < this.bolt_node.x) {
                            this.bolt.x = touch_x;
                            this.bolt_node.x = this.bolt.x;
                            this.is_bolt_back = false;
                            console.log(this.is_bolt_back);
                        }
                    }else {
                        if(this.cur_bullet_count > 0 && !this.bolt_is_finished) {
                            this.bolt.getChildByName('bullet').active = true;
                            this.cur_bole_state = BOLT_STATE.BOLE;
                            if(this.last_bullet_state == 'fire') {
                                this.last_bullet_state = '';
                                    let dir = this.randNum(0, 1) == 0 ? -1 : 1;
                                    let v1 = cc.v2(this.randNum(50, 200), this.randNum(500, 700));
                                    let v2 = cc.v2(this.randNum(v1.x, v1.x + 200), -1000);
                                    let _rotation = dir * this.randNum(70, 120);
                                    this.popup_Bullet(this.shell_pre, this.shell_spr, 0.4, v1, v2, _rotation);
                            }else if(!this.is_first_upmagazine) {
                                this.is_first_upmagazine = true;
                            }else if(this.last_bullet_state != 'fire' && this.is_first_upmagazine) {
                                //弹子弹
                                this.setBullet(this.cur_bullet_count - 1);
                                let dir = this.randNum(0, 1) == 0 ? -1 : 1;
                                let v1 = cc.v2(dir * this.randNum(100, 300), this.randNum(500, 700));
                                let v2 = cc.v2(dir * this.randNum(Math.abs(v1.x), Math.abs(v1.x) + 300), -1000);
                                let _rotation = dir * this.randNum(30, 80);
                                this.popup_Bullet(this.bullet_pre, this.bullet_spr, 0.4, v1, v2, _rotation);
                            }
                        }
                        if(!this.bolt_is_finished) {
                            this.bolt_is_finished = true;
                            this.playSound(AUDIO.BOLTBACK);
                        }
                        this.bolt.x = -this.bolt.width + 120;
                        this.bolt_node.x = this.bolt.x;
                    }
                }else {
                    if(this.bolt.x <= 0) {
                        if(touch_x > this.bolt_node.x) {
                            this.bolt.x = touch_x;
                            this.bolt_node.x = this.bolt.x;
                        }
                    }else {
                        if(!this.is_bolt_back) {
                            this.playSound(AUDIO.BOLTFOR);
                            this.is_bolt_back = true;
                        }
                        this.bolt.getChildByName('bullet').active = false;
                        this.bolt.x = 0.1;
                        this.bolt_node.x = 0.1;
                    }
                }
                break;
            default:
                break;
        }
    }

    
    protected onTouchEnd (event) : void {
        switch (this.cur_touch_name) {
            case 'bolt':
                // this.bolt_node.runAction(cc.moveTo(0.1, 0, this.bolt_node.y));
                // this.bolt.runAction(cc.moveTo(0.1, 0, this.bolt.y));
                //如果枪栓拉到底了并且回位了
                console.log(this.bolt_is_finished);
                if(this.bolt_is_finished && this.is_bolt_back) {
                    this.bolt_is_finished = false;
                    this.bolt_node.getChildByName('off').active = true;
                    this.bolt_node.getChildByName('on').active = false;
                    this.cur_bole_state = BOLT_STATE.BOLE;
                }else{
                    this.cur_bole_state = BOLT_STATE.NOBOLE;
                }
                break;
            case 'trigger':
                this.trigger_node.rotation = 0;
                this.cur_bole_state = BOLT_STATE.NOBOLE;
                break;
            case 'clip':
                if(this.cur_magazine_state == STATE.MAGAZINE) {
                    this.ani.play('DownMagazine');
                    setTimeout(() => {
                        this.playSound(AUDIO.DOWNMAGAZINE);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(0);
                }else{
                    this.last_bullet_state = '';
                    this.ani.play('UpMagazine');
                    this.playSound(AUDIO.UPMAGAZINE);
                    this.is_first_upmagazine = false;
                    this.cur_magazine_state = STATE.MAGAZINE;
                    this.setBullet(this.max_bullet_count);
                }
                break;
            case 'magazine':
                //上下弹匣
                if(this.touch_y - event.touch.getLocation().y > 0) {
                    if(this.cur_magazine_state == STATE.MAGAZINE) {
                        this.ani.play('DownMagazine');
                        setTimeout(() => {
                            this.playSound(AUDIO.DOWNMAGAZINE);
                        }, 300);
                        this.cur_magazine_state = STATE.NOMAGAZINE;
                        this.cur_bole_state = BOLT_STATE.NOBOLE;
                        this.setBullet(0);
                    }
                }else {
                    if(this.cur_magazine_state == STATE.NOMAGAZINE) {
                        this.ani.play('UpMagazine');
                        this.playSound(AUDIO.UPMAGAZINE);
                        this.is_first_upmagazine = false;
                        this.last_bullet_state = '';
                        this.cur_magazine_state = STATE.MAGAZINE;
                        this.setBullet(this.max_bullet_count);
   
                    }
                }
                break;
            case 'lock':
                //改变模式
                if(this.touch_y - event.touch.getLocation().y > 1) {
                    if(this.mode < MODE.SINGLE) {
                        this.lock_node.rotation += this.lock_rotation;
                        this.mode++;
                    }
                }else if(this.touch_y - event.touch.getLocation().y < -1){
                    if(this.mode > MODE.LOCK) {
                        this.lock_node.rotation -= this.lock_rotation;
                        this.mode--;
                    }
                }
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }
}
