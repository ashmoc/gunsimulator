// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import Banner from "./Banner";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    adContent: cc.Node = null;
  
    @property(cc.Node)
    Img: cc.Node = null;
  
    @property(cc.Node)
    Btn_Click: cc.Node = null;
  
    @property(cc.Node)
    Btn_Down: cc.Node = null;
  
    // @property(cc.Label)
    // lbl_ClickBtn: cc.Label = null;
  
    @property(cc.Node)
    Btn_Ad: cc.Node = null;
  
    @property(cc.Node)
    Btn_Close: cc.Node = null;
  
    @property(cc.Node)
    Btn_Close_BG: cc.Node = null;
  
    @property(cc.Node)
    lbl_Title: cc.Node = null;
  
    @property(cc.Node)
    lbl_Desc: cc.Node = null;
    // @property(cc.Node)
    // Btn_JumpAd: cc.Node = null;

      /**
   * 当前广告信息
   */
  current_Ad = null;

    // onLoad () {}

    start () {
       this.adContent.opacity = 0;
    let self = this;
    //创建oppo 原生广告组件
    console.log("创建 oppo 原生广告组件");
    this.display_Ad();
    // //加载原生广告，获取广告数据，成功回调 onLoad，失败回调 onError。
    // Banner.Instance.nativeAd.load();
    self.Btn_Close.on(cc.Node.EventType.TOUCH_END, function (event) {
      console.log("oppo  原生广告 点击 -----------> 关闭btn");
      self.off_YuanSheng();
    }, self)

    }

      /**
   * 展示广告
   ** adId	string	广告标识，用来上报曝光与点击
   ** title	string	广告标题
   ** desc	string	广告描述
   ** icon	string	推广应用的Icon图标
   ** imgUrlList	Array	广告图片，建议使用该图片资源
   ** logoUrl	string	广告标签图片
   ** clickBtnTxt	string	点击按钮文本描述
   ** creativeType	number	获取广告类型，取值说明：0：无 1：纯文字 2：图片 3：图文混合 4：视频 6. 640x320 大小图文混合 7. 320x210 大小图文单图 8. 320x210 大小图文多图
   ** interactionType	number	获取广告点击之后的交互类型，取值说明：0：无 1：浏览类 2：下载类 3：浏览器（下载中间页广告） 4：打开应用首页 5：打开应用详情页
   */
  display_Ad() {
    console.log("显示广告--------------------display_Ad");
    let self = this;
    // 原生广告在没有被曝光或者点击的情况下，再去拉去原生广告就会返回空广告
    // 所以load完要去执行reportAdShow和reportAdClick 才能正常拉取下一条广告
    // 注意：需要先调用reportAdShow上报广告曝光，才能调用reportAdClick去上报点击！！！
    self.report_Ad_Show();
    console.log("显示广告--------------------report_Ad_Show");
    let adId = Banner.Instance.adUnitAdid;           //广告标识，用来上报曝光与点击
    //let imgURL = self.current_Ad.imgUrlList[0];   //广告图片
    let imgURL =Banner.Instance.adUnitImgUrl;
    let title = Banner.Instance.title;         //广告标题
    let desc = Banner.Instance.clickBtnTxt;           //广告描述
    // let clickBtnTxt = self.current_Ad.clickBtnTxt;  //点击按钮文本描述
    // let interactionType = self.current_Ad.interactionType;  //获取广告点击之后的交互类型
    console.log("广告图片 imgURL ----------->" + imgURL);
    //没有图片路径
    if (!imgURL) {
      // this.off_YuanSheng();
      console.log("------> oppo原生没有广告图片路径");
    //   self.Img.opacity = 0;
    //   self.adContent.opacity = 0;
      Banner.Instance.yuansheng_Node.destroy();
    }

    self.lbl_Title.getComponent(cc.Label).string = title ? title : '';
    self.lbl_Desc.getComponent(cc.Label).string = desc ? desc : '';
    // self.lbl_ClickBtn.string = clickBtnTxt ? clickBtnTxt : '';
    self.Btn_Click.on(cc.Node.EventType.TOUCH_END, function (event) {
      console.log("Btn_Click ------------------> click 点击");
      self.report_Ad_Click();
     // self.off_YuanSheng();
    });
    //
    cc.loader.load({ url: imgURL, type: 'png' }, function (err, texture) {
      // Use texture to create sprite frame
      if (err) {
        console.log('------> 原生广告图片加载error ------> ', err.message || err);
        return;
      }

      console.log("原生广告图片-----------> 1");

      try {

        // 图片
        const sprite = new cc.SpriteFrame(texture);
        self.Img.addComponent(cc.Sprite).spriteFrame = sprite;

          self.Img.setContentSize(650, 400);

        self.adContent.opacity = 255;

        console.log("原生广告图片-----------> 2");
      } catch (e) {
        console.log("原生广告图片-----------> 3");
        self.adContent.opacity = 255;
      }
    });
  }

  /**
   * 上报广告曝光
   */
  report_Ad_Show() {
    let self = this;
    Banner.Instance.nativeAd.reportAdShow({
      adId: Banner.Instance.adUnitAdid
    });
  }
  /**
   * 上报广告点击
   */
  report_Ad_Click() {
      console.log("广告点击"+Banner.Instance.adUnitAdid);
    let self = this;
    Banner.Instance.nativeAd.reportAdClick({
      adId: Banner.Instance.adUnitAdid 
    });
    self.off_YuanSheng();
  }
    // update (dt) {}
    off_YuanSheng() {
        console.log("oppo 原生广告 销毁");
       // oppo_Ad_Data.isYuanSheng = false;
        if (Banner.Instance.nativeAd) {
            Banner.Instance.nativeAd.offLoad();
            Banner.Instance.nativeAd.offError();
        }
        Banner.Instance.nativeAd.destroy()
        Banner.Instance.nativeAd = null;
        // if (oppo_Ad_Data.YuanSheng_Node) {
        //   oppo_Ad_Data.YuanSheng_Node.destroy();
        // }

        // oppo_Ad_Data.YuanSheng_Node = null;
    
        this.node.destroy();
    
      }
}
