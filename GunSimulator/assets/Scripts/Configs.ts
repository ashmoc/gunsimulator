export var Configs = {
    coin : 0,
    is_first_into_game : true,
    video_name : '',
    add_coin : 4000,
    gun_name : '',
    bullet : 1,
    add_bullet : 10,
};
export const MENU = {
    GAME_MENU : 'Scenes/GameMenu',
    GAME_LOADING : 'Scenes/GameLoading',
    GAME_PLAY : 'Scenes/GamePlay',
    GAME_START : 'Scenes/GameStart',
}

export const CLIP = {
    RECOIL : 0,
    DOWNMAGAZINE : 1,
    UPMAGAZINE : 2,
    FIRE : 3,
}

export const AUDIO = {
    FIRE : 0,
    UPMAGAZINE : 1,
    DOWNMAGAZINE : 2,
    TRIGGER : 3,
    LOCK : 4,
    BOLTBACK : 5,
    BOLTFOR : 6,
}

export const MODE = {
    LOCK : 0,
    CONTINUOUS : 1,
    SINGLE : 2,
}

export const STATE = {
    NOMAGAZINE : 0,
    MAGAZINE : 1,
}

export const BOLT_STATE = {
    NOBOLE : 0,
    BOLE : 1,
}