import GUN from "./GUN";
import { MODE, STATE } from "./Configs";
import AudioManager from "./AudioManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Vulcan extends GUN {


    @property(cc.Sprite)
    private Order_1 : cc.Sprite = null;

    // private static _instance: any;
    // public static get Instance(): Vulcan {
    //     if (Vulcan._instance == null)
    //     Vulcan._instance = new Vulcan();
    //     return Vulcan._instance;
    // }
    // onLoad(){
    //     Vulcan._instance = this;
    // }

    
    Change()
        {
            if (cc.sys.platform == cc.sys.ANDROID) {
                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "ChaPing2", "()V");
            }
            var self = this;
                cc.loader.loadRes("Image/Gun/Vulcan/new_1",cc.SpriteFrame, function (err, Frame)
            {
                self.Order_1.spriteFrame = Frame;
            })
           
            
        }

    protected isCanFire () : boolean {
        if(this.cur_bullet_count > 0 && this.mode == MODE.CONTINUOUS) {
            return true;
        }
        return false;
    }

    private stopAni () : void {
        this.ani.stop('Barrels');
        AudioManager.Instance.stopSound();
        this.playSound(1);
    }

    private playWorkingSound () {
        this.playSound(3);
    }

    private Fire () : void {
        if(this.isCanFire()) {
            var shell = cc.instantiate(this.shell_pre);
            this.bullet_node.addChild(shell);
            shell.getChildByName('Img').getComponent(cc.Sprite).spriteFrame = this.shell_spr;
            shell.runAction(cc.sequence(cc.spawn(
                cc.moveTo(0.4, this.randNum(-100, 100), -800),
                cc.callFunc( (_node) => {
                    _node.rotation = this.randNum(20, 50);
                }, this)), 
                cc.callFunc( (_node) => {
                    _node.destroy();
                }, this)
            ))
            this.fireLight();
            this.setBullet(this.cur_bullet_count - 1);
            this.playSound(0);
        }else {
            this.fireLight(false);
            this.unschedule(this.Fire);
        }
    }

    protected onTouchStart (event) {
        let lock_rect = null;
        let trigger_rect = null;
        let magazine_rect = null;
        if(this.lock_node && this.cur_touch_name == '') {
            lock_rect = this.lock_node.getBoundingBoxToWorld();
            if(lock_rect.contains(event.touch.getLocation())) {
                this.cur_touch_name = 'lock';
                if(this.mode == MODE.CONTINUOUS) {
                    this.mode = MODE.LOCK;
                    this.lock_node.rotation = 0;
                    this.scheduleOnce(this.stopAni, 0.5)
                    this.unschedule(this.playWorkingSound);
                }else {
                    this.playSound(2);
                    this.schedule(this.playWorkingSound, 0.1);
                    this.lock_node.rotation = this.lock_rotation;
                    this.mode = MODE.CONTINUOUS;
                    this.unschedule(this.stopAni);
                    this.ani.playAdditive('Barrels');
                }
            }
        }
        if(this.trigger_node && this.cur_touch_name == '') {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if(trigger_rect.contains(event.touch.getLocation())) {
                this.cur_touch_name = 'trigger';
                this.trigger_node.rotation = this.trigger_rotation;
                if(this.isCanFire()) {
                    this.node.stopAllActions();
                    this.node.runAction(cc.moveTo(0.2, -400, 0));
                    this.Fire();
                    this.schedule(this.Fire, 0.03);
                }
            }
        }
        if(this.magazine_node && this.cur_touch_name == '') {
            magazine_rect = this.magazine_node.getBoundingBoxToWorld();
            if(magazine_rect.contains(event.touch.getLocation())) {
                this.cur_touch_name = 'magazine';
            }
        }
        this.touch_y = event.touch.getLocation().y;
    }

    protected onTouchEnd (event) {
        switch (this.cur_touch_name) {
            case 'magazine':
                let end_y = event.touch.getLocation().y;
                if(this.touch_y - end_y > 1) {
                    this.ani.playAdditive('DownMagazine');
                    setTimeout(() => {
                        this.playSound(5);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(0);
                }else if(this.touch_y - end_y < -1) {
                    this.ani.playAdditive('UpMagazine');
                    setTimeout(() => {
                        this.playSound(4);
                    }, 300);
                    this.cur_magazine_state = STATE.NOMAGAZINE;
                    this.setBullet(this.max_bullet_count);
                }
                break;
            case 'trigger':
                this.trigger_node.rotation = 0;
                this.fireLight(false);
                this.unschedule(this.Fire);
                this.node.runAction(cc.moveTo(0.2, -385, 0));
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
    }

}