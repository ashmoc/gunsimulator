import Tools from "./Tools";
import { MENU, Configs } from "./Configs";
import Banner from "./Banner";
import Const from "./Taser/Const";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameStart extends Tools {

    @property({ type: cc.Label, tooltip: '金币显示文字' })
    private lab_coin: cc.Label = null;

    @property({ type: cc.Label, tooltip: '子弹倍数显示文字' })
    private lab_bullet: cc.Label = null;

    @property({ type: cc.Node, tooltip: '七日登录节点' })
    private days_node: cc.Node = null;


    @property({ type: cc.Node, tooltip: '奖励面板' })
    private reward_panel: cc.Node = null;

    @property({ type: cc.SpriteFrame, tooltip: '奖励的图片' })
    private reward_spr_arr: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();

    //转盘次数
    private lucky_count: number = 0;

    //可领取天数的背景灯光
    private light: cc.Node = null;

    //是否有正在执行的动画
    private is_actioning: boolean = false;

    //当前点击的y坐标（转盘用）
    private touch_start_y: number = 0;

    //当前打开的奖励面板的名字
    private cur_open_gift_name: string = '';

    //随机到的奖励
    private rand_gift: number = -1;

    /**奖励金币 */
    reward_coin: string;

    private static _instance: any;
    public static get Instance(): GameStart {
        if (GameStart._instance == null)
            GameStart._instance = new GameStart();
        return GameStart._instance;
    }
    onLoad() {
        GameStart._instance = this;
        this.coinActive();
    }

    coinActive() {
        if (cc.sys.localStorage.getItem('Coin') != null && cc.sys.localStorage.getItem('Coin') != "nan" && cc.sys.localStorage.getItem('Coin') != "") {
            this.lab_coin.string = cc.sys.localStorage.getItem('Coin');
        } else {
            this.lab_coin.string = "0";
        }
    }

    start() {
        //Banner.Instance.showBanner();
        this.reward_panel.active = false;
        // this.InitData();
        this.ReadData();
        this.days_node.parent.active = false;

        this.lucky_count = this.GetData('lucky_count');
        if (!this.GetData('coin')) {
            this.SetData('coin', 0);
            this.WriteData();
        }
        if (!this.GetData('bullet')) {
            this.SetData('bullet', 0);
            this.WriteData();
        }
        Configs.bullet = this.GetData('bullet');
        this.lab_bullet.string = 'x' + Configs.bullet;
        Configs.coin = this.GetData('coin');
        // this.lab_coin.string = '' + Configs.coin;

        // this.InitData();
        //获取当前时间 判断上一次进入游戏到现在过了几天
        // if (this.GetData('last_day') >= 7) {
        //     this.node.getChildByName('Right_Down').getChildByName('Btn_Days').active = false;
        // } else {
        //     this.showDaysBG();
        //     //点击面板周围空白关闭此面板
        //     this.days_node.parent.on(cc.Node.EventType.TOUCH_END, (event) => {
        //         if (!this.is_actioning) {
        //             this.is_actioning = true;
        //             this.cur_open_gift_name = '';
        //             event.target.runAction(cc.sequence(cc.scaleTo(0.3, 0), cc.callFunc((_node) => {
        //                 _node.active = false;
        //                 this.is_actioning = false;
        //             }, this)));
        //         }
        //     }, this);
        // }

    }

    CoinBack() {
        Configs.video_name = 'coin';
        GameStart.Instance.PlayVideo((success: boolean) => {
            if (Configs.video_name == 'coin' && success) {
                Configs.video_name = '';
                GameStart.Instance.addCoin(Configs.add_coin, GameStart.Instance.lab_coin);
            }
        })
    }
    BulletBack() {
        Configs.video_name = 'bullet';
        GameStart.Instance.PlayVideo((success: boolean) => {
            if (Configs.video_name == 'bullet' && success) {
                Configs.video_name = '';
                GameStart.Instance.addBullet(Configs.add_bullet, GameStart.Instance.lab_bullet);
            }
        })
    }
    RewardBack() {

        if (cc.sys.localStorage.getItem("Coin") != null || cc.sys.localStorage.getItem("Coin") != "" || cc.sys.localStorage.getItem("Coin") != "NaN") {
            let a = parseInt(cc.sys.localStorage.getItem("Coin")) + parseInt(GameStart.Instance.reward_coin) * 2;
            cc.sys.localStorage.setItem("Coin", a);
        } else {
            let a = parseInt(GameStart.Instance.reward_coin) * 2;
            GameStart.Instance.reward_coin = a.toString();
            cc.sys.localStorage.setItem("Coin", GameStart.Instance.reward_coin);


        }
        let a = parseInt(GameStart.Instance.reward_coin) * 2;

        GameStart.Instance.lab_coin.string = a.toString();
        GameStart.Instance.reward_panel.active = false;
        GameStart.Instance.reward_panel.active = false;

    }

    onGiftBtn() {

    }


    //按钮事件
    public Btn_Click(event, data): void {
        switch (event.target.name) {
            case 'Btn_Start':

                let loading = this.LoadScene(MENU.GAME_LOADING, cc.find('Canvas'));
                loading.then((_value: cc.Node) => {
                    _value.getComponent('GameLoading').load(MENU.GAME_MENU);
                    this.RemoveScene('GameStart');
                })
                Banner.Instance.CreateCustomAd();
                break;
            // case 'Btn_Gift':

            //     this.getBoxGift();
            //     break;
            case 'Coin':
                Banner.Instance.CreateVideo(this.CoinBack);
                break;
            case 'Bullet':
                Banner.Instance.CreateVideo(this.BulletBack);
                break;
            case 'Lab_Lucky_Count':

                break;
            case 'Btn_Days':
                if (!this.is_actioning) {

                    this.is_actioning = true;
                    this.cur_open_gift_name = 'day';
                    this.days_node.parent.active = true;
                    this.days_node.parent.scale = 0;
                    this.days_node.parent.runAction(cc.sequence(cc.scaleTo(0.3, 1), cc.callFunc(() => {
                        this.is_actioning = false;
                    }, this)));
                }
                break;
            case 'Btn_Lucky':

                break;
            case 'Btn_Get_Double_Reward':
                Banner.Instance.CreateVideo(this.RewardBack);
                break;
            case 'Btn_Get_Reward':
                let lab = this.reward_panel.getChildByName('BG').getChildByName('Lab_Tips copy').getComponent(cc.Label);
                // GameStart.Instance.addCoin(parseInt(lab.string), this.lab_coin);
                cc.sys.localStorage.setItem("Coin", lab);
                this.lab_coin.string = lab.string;
                GameStart.Instance.reward_panel.active = false;
                this.reward_panel.active = false;
                break;

            case 'CloseHide':
                break;

            case 'Btn_Return':
                cc.director.loadScene(Const.SceneName.Menu);
                break;

            default:
                break;

        }
    }

    onGetReward() {
        if (cc.sys.localStorage.getItem("Coin") != null || cc.sys.localStorage.getItem("Coin") != "" || cc.sys.localStorage.getItem("Coin") != "NaN") {
            let a = parseInt(cc.sys.localStorage.getItem("Coin")) + parseInt(this.reward_coin);
            cc.sys.localStorage.setItem("Coin", a);
        } else {
            cc.sys.localStorage.setItem("Coin", this.reward_coin);

        }
        this.lab_coin.string = this.reward_coin;
        GameStart.Instance.reward_panel.active = false;
        this.reward_panel.active = false;
    }

    onGetDoubleReward() {
        Banner.Instance.CreateVideo(this.RewardBack);

    }


    private showDaysBG() {
        //如果是刚刚进入游戏就自动打开7日登陆
        if (Configs.is_first_into_game) {
            Configs.is_first_into_game = false;
            this.is_actioning = true;
            this.days_node.parent.active = true;
            this.days_node.parent.scale = 0;
            this.days_node.parent.runAction(cc.sequence(cc.scaleTo(0.3, 1), cc.callFunc(() => {
                this.is_actioning = false;
            }, this)));
            this.cur_open_gift_name = 'day';
        }
        //判断是否过了一天时间
        let last_day: number = this.GetData('last_day');
        let date_now: Date = new Date();
        console.log(this.GetData('last_into_game_time'));
        let last_into_game_time = this.GetData('last_into_game_time').split('.');

        // let date : number = new Date().getTime();
        // let delta : number = date - this.GetData('last_into_game_time');
        // let days : number = parseInt((delta / (1000 * 60 * 60 * 24)).toFixed(0));
        // let days : number = parseInt(((delta / 1000) / 84600).toFixed(0));
        this.light = this.days_node.parent.getChildByName('light');
        console.log('当前是哪一天', date_now.getMonth() + '.' + date_now.getDate());
        console.log('上一次签到是第几天', last_day);

        for (let i: number = 0; i < last_day; i++) {
            this.days_node.children[i].getChildByName('Img').active = true;
        }
        //如果时间没有超过1天
        if (last_into_game_time[0] == date_now.getMonth() && last_into_game_time[1] == date_now.getDate()) {
            this.light.active = false;
        } else {
            //设置高亮的天数并注册点击事件
            this.light.x = this.days_node.children[last_day].x;
            this.days_node.children[last_day].once(cc.Node.EventType.TOUCH_START, (event) => {
                this.SetData('last_into_game_time', (date_now.getMonth() + '.' + date_now.getDate()));
                this.SetData('last_day', last_day + 1);
                let pass: cc.Node = event.target.getChildByName('Img');
                pass.active = true;
                pass.y += pass.height;
                pass.runAction(cc.moveBy(0.2, 0, -pass.height));
                this.WriteData();
                this.showDaysGift(last_day);
            }, this);
        }
        // if(days < 1) {
        //     this.light.active = false;
        // }else {
        //     //设置高亮的天数并注册点击事件
        //     this.light.x = this.days_node.children[last_day].x;
        //     this.days_node.children[last_day].once(cc.Node.EventType.TOUCH_START, (event) => {
        //         this.SetData('last_into_game_time', (date_now.getMonth() + '.' + date_now.getDate()));
        //         this.SetData('last_day', last_day + 1);
        //         let pass : cc.Node = event.target.getChildByName('Img');
        //         pass.active = true;
        //         pass.y += pass.height;
        //         pass.runAction(cc.moveBy(0.2, 0, -pass.height));
        //         this.WriteData();
        //         this.showDaysGift(last_day);
        //     }, this);
        // }
    }

    private showDaysGift(index: number): void {
        if (this.is_actioning) return;
        let lab = this.reward_panel.getChildByName('BG').getChildByName('Lab_Tips copy').getComponent(cc.Label);
        switch (index) {
            case 0:
                lab.string = '恭喜获得1000金币！！！';
                this.reward_coin = "1000";
                break;
            case 1:
                lab.string = '恭喜获得2000金币！！！';
                this.reward_coin = "2000";
                break;
            case 2:
                lab.string = '恭喜获得3000金币！！！';
                this.reward_coin = "3000";
                break;
            case 3:
                lab.string = '恭喜获得4000金币！！！';
                this.reward_coin = "4000";
                break;
            case 4:
                lab.string = '恭喜获得5000金币！！！';
                this.reward_coin = "5000";
                break;
            case 5:
                lab.string = '恭喜获得6000金币！！！';
                this.reward_coin = "6000";
                break;
            case 6:
                lab.string = '恭喜获得7000金币！！！';
                this.reward_coin = "7000";
                break;
            default:
                break;
        }
        this.is_actioning = true;
        this.reward_panel.active = true;
        this.reward_panel.scale = 0;
        this.unschedule(this.showRewardClose);
        this.reward_panel.getChildByName('BG').getChildByName('Btn_Get_Reward').active = false;
        this.reward_panel.runAction(cc.sequence(cc.scaleTo(0.3, 1), cc.callFunc(() => { this.is_actioning = false; }, this)));
        this.scheduleOnce(this.showRewardClose, 3);

    }

    private showLuckyGift(index: number): void {
        if (this.is_actioning) return;
        let lab = this.reward_panel.getChildByName('BG').getChildByName('Lab_Tips copy').getComponent(cc.Label);
        switch (index) {
            case 0:
                lab.string = '恭喜获得1000金币！！！';
                this.reward_coin = "1000";
                break;
            case 1:
                lab.string = '恭喜获得2000金币！！！';
                this.reward_coin = "2000";
                break;
            case 2:
                lab.string = '恭喜获得3000金币！！！';
                this.reward_coin = "3000";
                break;
            case 3:
                lab.string = '恭喜获得4000金币！！！';
                this.reward_coin = "4000";
                break;
            case 4:
                lab.string = '恭喜获得5000金币！！！';
                this.reward_coin = "5000";
                break;
            case 5:
                lab.string = '恭喜获得6000金币！！！';
                this.reward_coin = "6000";
                break;
            case 6:
                lab.string = '恭喜获得7000金币！！！';
                this.reward_coin = "7000";
                break;
            default:
                break;
        }
        this.is_actioning = true;
        this.reward_panel.active = true;
        this.reward_panel.scale = 0;
        this.unschedule(this.showRewardClose);
        this.reward_panel.getChildByName('BG').getChildByName('Btn_Get_Reward').active = false;
        this.reward_panel.runAction(cc.sequence(cc.scaleTo(0.3, 1), cc.callFunc(() => { this.is_actioning = false; }, this)));
        this.scheduleOnce(this.showRewardClose, 3);
    }

    showRewardClose() {
        this.reward_panel.getChildByName('BG').getChildByName('Btn_Get_Reward').active = true;
    }

    onAddLuckyCount() {

    }

    private randRotation(): number {
        let probability: Array<number> = [25, 10, 19, 2, 25, 12, 5, 2];
        let rand: number = this.randNum(1, 100);
        let factor: number = 0;
        for (let i: number = 0; i < probability.length; i++) {
            factor += probability[i];
            if (rand <= factor) {
                return i;
            }
        }
        return null;
    }

    private getBoxGift(): void {
        // this.cur_open_gift_name = 'boxgift';
        // Configs.video_name = 'boxgift';
        Banner.Instance.CreateVideo(this.getBoxGiftBack);

    }

    getBoxGiftBack() {
        //随机出奖励
        GameStart.Instance.rand_gift = GameStart.Instance.randRotation();
        GameStart.Instance.showLuckyGift(GameStart.Instance.rand_gift);
    }

    private get_day_reward(_time: number = 0) {
        let day: number = this.GetData('last_day');
        console.log(day);
        switch (day) {
            case 1:
                this.addCoin(3000, this.lab_coin);
                break;
            case 2:
                this.addCoin(5000, this.lab_coin);
                break;
            case 3:
                if (_time == 0) {
                    this.rand_skin_gun();
                } else {
                    this.addCoin(5000, this.lab_coin);
                }
                break;
            case 4:
                this.addBullet(10, this.lab_bullet);
                break;
            case 5:
                this.addCoin(6000, this.lab_coin);
                break;
            case 6:
                this.addBullet(20, this.lab_bullet);
                break;
            case 7:
                if (_time == 0) {
                    this.rand_skin_gun();
                } else {
                    this.addCoin(10000, this.lab_coin);
                }
                break;
            default:
                break;
        }
    }

    private get_lucky_reward(_time: number = 0): void {
        switch (this.rand_gift) {
            case 0:
                this.addBullet(5, this.lab_bullet);
                break;
            case 1:
                this.addCoin(4000, this.lab_coin);
                break;
            case 2:
                this.addBullet(20, this.lab_bullet);
                break;
            case 3:
                if (_time == 0) {
                    this.rand_skin_gun();
                } else {
                    this.addCoin(10000, this.lab_coin);
                }
                break;
            case 4:
                this.addCoin(2000, this.lab_coin);
                break;
            case 5:
                this.addBullet(10, this.lab_bullet);
                break;
            case 6:
                this.addCoin(6000, this.lab_coin);
                break;
            case 7:
                if (_time == 1) {
                    this.rand_normal_gun();
                } else {
                    this.addCoin(5000, this.lab_coin);
                }
                break;
            default:
                break;
        }
    }

    //随机一把普通的枪并且解锁
    private rand_normal_gun(): string {
        let unlock_gun_arr = this.unLock();
        if (unlock_gun_arr.length <= 0) {
            //TODO
            //显示面板显示没有未解锁的普通枪支 给金币

            return;
        }
        //将未解锁枪中的带皮肤的枪去除
        for (let i: number = 0; i < unlock_gun_arr.length; i++) {
            let a = unlock_gun_arr[i].split('_');
            if (a[1] != undefined) {
                unlock_gun_arr.splice(i, 1);
                i--;
            }
        }
        //随机一把枪并解锁
        let rand: number = this.randNum(0, unlock_gun_arr.length - 1);
        let gun_data = this.GetData('gun_data');
        gun_data[unlock_gun_arr[rand]]['lock'] = false;
        this.WriteData();
        console.log(unlock_gun_arr[rand]);
        return unlock_gun_arr[rand];
    }

    //随机一把带皮肤的枪
    private rand_skin_gun(): string {
        let unlock_gun_arr = this.unLock();
        if (unlock_gun_arr.length <= 0) {
            //TODO
            //显示面板显示没有未解锁的普通枪支 给金币
            return;
        }
        //将未解锁枪中的普通的枪去除
        for (let i: number = 0; i < unlock_gun_arr.length; i++) {
            let a = unlock_gun_arr[i].split('_');
            if (a[1] == undefined) {
                unlock_gun_arr.splice(i, 1);
                i--;
            }
        }
        //随机一把枪并解锁
        let rand: number = this.randNum(0, unlock_gun_arr.length - 1);
        let gun_data = this.GetData('gun_data');
        gun_data[unlock_gun_arr[rand]]['lock'] = false;
        this.WriteData();
        console.log(unlock_gun_arr[rand]);
        return unlock_gun_arr[rand];
    }
}