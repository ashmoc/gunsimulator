import GUN from "./GUN";
import { CLIP, AUDIO } from "./Configs";

const {ccclass, property} = cc._decorator;

@ccclass
export default class M203 extends GUN {
    @property({type : cc.Node, tooltip : '子弹'})
    private bullet : cc.Node = null;

    @property({type : cc.Node, tooltip : '后座'})
    private butt : cc.Node = null;

    private touch_x : number = 0;                       //手指按下的点的x

    private is_butt_move : boolean = false;             //后座是否是在移动中

    private is_bolt_move : boolean = false;             //枪栓是否是在移动中

    private is_bolt_open : boolean = false;             //枪栓是否是打开的

    private is_bullet_finished : boolean = false;       //子弹是否装填完毕

    protected isCanFire() : boolean {
        if(this.is_bullet_finished && !this.is_bolt_open) {
            this.bullet.active = false;
            this.bullet.zIndex = 1;
            this.bullet.position = cc.v2(140, -250);
            this.is_bolt_move = true;
            setTimeout(() => {
                this.is_bolt_move = false;
            }, 500);
            return true;
        }
        return false;
    }

    // private static _instance: any;
    // public static get Instance(): M203 {
    //     if (M203._instance == null)
    //     M203._instance = new M203();
    //     return M203._instance;
    // }
    // onLoad(){
    //     M203._instance = this;
    // }

    protected onTouchStart (event) : void {
        let trigger_rect = null;
        let butt_rect = null;
        let bolt_rect = null;
        let bullet_rect = null;
        if(this.bullet && !this.is_bullet_finished) {
            bullet_rect = this.bullet.getBoundingBoxToWorld();
            if(bullet_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('bullet');
                this.cur_touch_name = 'bullet';
            }
        }
        if(this.bolt_node) {
            bolt_rect = this.bolt_node.getBoundingBoxToWorld();
            if(bolt_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('bolt');
                this.cur_touch_name = 'bolt';
            }
        }
        if(this.trigger_node) {
            trigger_rect = this.trigger_node.getBoundingBoxToWorld();
            if(trigger_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('trigger');
                this.cur_touch_name = 'trigger';
                this.trigger_node.rotation = this.trigger_rotation;
                if(this.isCanFire()) {
                    this.ani.play('M203_Fire');
                    this.setBullet(0);
                    this.is_bullet_finished = false;
                    setTimeout(() => {
                        this.playSound(AUDIO.FIRE);
                        this.singleFireLight();
                    }, 500);
                    setTimeout(() => {
                        this.playSound(6);
                    }, 1500);
                }else {
                    this.playSound(AUDIO.TRIGGER);
                }
            }
        }
        if(this.butt) {
            butt_rect = this.butt.getBoundingBoxToWorld();
            if(butt_rect.contains(event.touch.getLocation()) && this.cur_touch_name == '') {
                console.log('butt');
                this.cur_touch_name = 'butt';
            }
        }
        this.touch_x = event.touch.getLocation().x;
    }

    protected onTouchMove(event) : void  {
        let delta = event.touch.getDelta();
        if(this.cur_touch_name == 'bullet' && !this.is_bullet_finished) {
            this.bullet.x += delta.x;
            this.bullet.y += delta.y;
        }
    }

    protected onTouchEnd(event) : void {
        switch (this.cur_touch_name) {
            case 'trigger':
                this.trigger_node.rotation = 0;
                break;
            case 'butt':
                if(!this.is_butt_move) {
                    this.butt.stopAllActions();
                    this.is_butt_move = true;
                    if(this.touch_x < event.touch.getLocation().x) {
                        this.butt.runAction(cc.sequence(cc.moveTo(0.3, -265, 40), cc.callFunc(() => {
                            this.is_butt_move = false;
                        }, this)));
                    }else {
                        this.butt.runAction(cc.sequence(cc.moveTo(0.3, -562, 40), cc.callFunc(() => {
                            this.is_butt_move = false;
                        }, this)));
                    }
                    setTimeout(() => {
                        this.playSound(4);
                    }, 300);
                }
                break;
            case 'bolt':
                if(!this.is_bolt_move) {
                    this.bolt_node.stopAllActions();
                    this.is_bolt_move = true;
                    if(this.touch_x < event.touch.getLocation().x) {
                        this.bolt_node.runAction(cc.sequence(cc.moveTo(0.3, 230, 0), cc.callFunc((_node) => {
                            this.is_bolt_move = false;
                            this.is_bolt_open = true;
                            if(_node.getChildByName('Shell').active && !this.bullet.active) {
                                this.ani.play('M203_Popup_Bullet');
                            }
                        }, this)));
                        this.playSound(1);
                    }else {
                        this.bolt_node.runAction(cc.sequence(cc.moveTo(0.3, 0, 0), cc.callFunc(() => {
                            this.is_bolt_move = false;
                            this.is_bolt_open = false;
                            this.node.runAction(cc.sequence(cc.rotateTo(0.1, -0.3), cc.rotateTo(0.1, 0)));
                        }, this)));
                        if(this.bolt_node.getChildByName('Shell').active) {
                            this.playSound(2);
                        }else {
                            this.playSound(1);
                        }
                    }
                }
                break;
            case 'bullet':
                if(this.is_bolt_open) {
                    if(this.bullet.getBoundingBox().contains((cc.v2(80, -35)))) {
                        this.setBullet(1);
                        this.bullet.runAction(cc.sequence(
                            cc.spawn(cc.rotateTo(0.2, 0), 
                            cc.moveTo(0.2, 80, -35)),
                            cc.delayTime(0.1),
                            cc.callFunc( (_node) => {_node.zIndex = -1}),
                            cc.moveTo(0.2, 300, -35), 
                            cc.callFunc((_node) => {
                                this.bolt_node.getChildByName('Shell').active = true;
                                this.playSound(2);
                            })
                        ));
                        this.is_bullet_finished = true;
                    }else {
                        this.bullet.stopAllActions();
                        this.bullet.runAction(cc.moveTo(0.3, 140, -250));
                    }
                }else {
                    this.bullet.stopAllActions();
                    this.bullet.runAction(cc.moveTo(0.3, 140, -250));
                }
                break;
            default:
                break;
        }
        this.cur_touch_name = '';
        console.log(this.cur_touch_name)
    }

}
