import Tools from "./Tools";
import { MENU } from "./Configs";
import StorageManager from "../_Taser/Scripts/StorageManager";
import Const from "../_Taser/Scripts/Const";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameMain extends Tools {

    private static instance: GameMain = null;
    public static get Instance(): GameMain {
        if (this.instance == null) {
            this.instance = new GameMain();
        }
        return this.instance;
    }

    @property({ type: cc.AudioClip, displayName: '背景音乐' })
    private bg_clip: cc.AudioClip = null;

    private audio_id: number = null;

    private effect_id: number = null;

    onLoad() {
        GameMain.instance = this;

        let loading = this.LoadScene(MENU.GAME_LOADING, cc.find('Canvas'));
        loading.then((_value: cc.Node) => {
            _value.getComponent('GameLoading').load(MENU.GAME_MENU);
            //this.RemoveScene('GameStart');
        })

        // let loading = this.LoadScene(MENU.GAME_LOADING, this.node);
        // loading.then((_value: cc.Node) => {
        //     _value.getComponent('GameLoading').load(MENU.GAME_START);
        // })
    }

    start() {
        // this.play();
        // this.node.getChildByName('Scenes').scaleX = (cc.view.getCanvasSize().width * 2) / cc.view.getVisibleSize().width;
        // this.node.getChildByName('Scenes').scaleY = (cc.view.getCanvasSize().height * 2) / cc.view.getVisibleSize().height;
        // console.log(this.node.getChildByName('Scenes').scaleX, this.node.getChildByName('Scenes').scaleY);
    }

    play() {
        this.audio_id = cc.audioEngine.playMusic(this.bg_clip, true);
    }

    pause() {
        cc.audioEngine.pauseMusic();
    }

    resume() {
        cc.audioEngine.resumeMusic();
    }

    playEffect(name: string) {
        if (cc.audioEngine.getEffectsVolume() <= 0)
            return;

        // if(name == 'clear') {
        //     this.effect_id = cc.audioEngine.playEffect(this.clear_clip, false);
        // }else if (name == 'move') {
        //     this.effect_id = cc.audioEngine.playEffect(this.move_clip, false);
        // }
    }

    offEffect() {
        cc.audioEngine.setEffectsVolume(0);
    }

    onEffect() {
        cc.audioEngine.setEffectsVolume(1);
    }

}
