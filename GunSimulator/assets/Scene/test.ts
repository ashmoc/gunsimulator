const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    start () {
        let date = new Date();
        console.log(date.getMonth());
    }

    Btn_Click () {
        var bezier = [cc.v2(0, 0), cc.v2(-100, 300), cc.v2(-200, -300)];
        var bezierTo = cc.bezierTo(2, bezier);
        this.node.getChildByName('bullet').runAction(bezierTo);
    }
}
